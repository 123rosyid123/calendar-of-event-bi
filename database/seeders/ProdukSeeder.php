<?php

namespace Database\Seeders;

use App\Models\Produk;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::factory(5)->create();
        foreach (Produk::all() as $produk) {
            $users = User::inRandomOrder()->take(rand(1, 3))->pluck('id');
            $produk->users()->attach($users);
        }
    }
}
