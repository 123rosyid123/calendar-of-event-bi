<?php

namespace Database\Seeders\settings;

use App\Models\SettingSkalaEvent;
use Illuminate\Database\Seeder;

class SettingSkalaEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(['lokal','nasional','internasional']as $skala){
            SettingSkalaEvent::create(["title" => $skala]);
        }
    }
}
