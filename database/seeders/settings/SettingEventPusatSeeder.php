<?php

namespace Database\Seeders\settings;

use App\Models\SettingEventPusat;
use Illuminate\Database\Seeder;

class SettingEventPusatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingEventPusat::factory(1)->create();
    }
}
