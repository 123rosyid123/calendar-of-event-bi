<?php

namespace Database\Seeders\settings;

use App\Models\SettingKemitraanKolaborasi;
use Illuminate\Database\Seeder;

class SettingKemitraanKolaborasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingKemitraanKolaborasi::factory(6)->create();
    }
}
