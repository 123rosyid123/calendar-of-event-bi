<?php

namespace Database\Seeders\settings;

use App\Models\SettingUndanganInternal;
use Illuminate\Database\Seeder;

class SettingUndanganInternalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingUndanganInternal::factory(1)->create();
    }
}
