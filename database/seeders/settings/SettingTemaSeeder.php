<?php

namespace Database\Seeders\settings;

use App\Models\SettingTema;
use Illuminate\Database\Seeder;

class SettingTemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingTema::factory(1)->create();
    }
}
