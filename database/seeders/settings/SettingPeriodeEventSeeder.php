<?php

namespace Database\Seeders\settings;

use App\Models\SettingPeriodeEvent;
use Illuminate\Database\Seeder;

class SettingPeriodeEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['pendaftaran', 'penyesuaian'] as $nama_periode) {
            SettingPeriodeEvent::create([
                'tanggal_mulai_pendaftaran' => null,
                'tanggal_akhir_pendaftaran' => null,
                'nama_periode' => $nama_periode,
                'is_active' => false,
            ]);
        }
    }
}
