<?php

namespace Database\Seeders\settings;

use App\Models\SettingFlagshipNasional;
use Illuminate\Database\Seeder;

class SettingFlagshipNasionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['GBBI', 'KKI', 'FEKDI', 'GWBI', 'Fesyar', 'ISEF'] as  $flagship_nasional) {
            SettingFlagshipNasional::create(['title' => $flagship_nasional]);
            # code...
        }
    }
}
