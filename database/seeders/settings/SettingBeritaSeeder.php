<?php

namespace Database\Seeders\settings;

use App\Models\SettingBerita;
use App\Models\User;
use Illuminate\Database\Seeder;

class SettingBeritaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingBerita::factory(2)->create();
    }
}
