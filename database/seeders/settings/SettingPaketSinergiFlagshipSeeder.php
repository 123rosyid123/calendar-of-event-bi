<?php

namespace Database\Seeders\settings;

use App\Models\SettingPaketSinergiFlagship;
use Illuminate\Database\Seeder;

class SettingPaketSinergiFlagshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'paket1'=> [1,2,3], //'GBBI', 'KKI', 'FEKDI'
            'paket2'=> [1,2,3,4], //'GBBI', 'KKI', 'FEKDI', 'GWBI'
            'paket3'=> [1,2,3,5], //'GBBI', 'KKI', 'FEKDI', 'Fesyar'
            'paket4'=> [1,2,3,4,5], //'GBBI', 'KKI', 'FEKDI', 'GWBI', 'Fesyar'
        ];
        foreach ($data as $paket => $detail) {
            $data = SettingPaketSinergiFlagship::create(['title' => $paket, 'detail' => 'detail']);
            $data->flagship_nasional()->attach($detail);
        }
    }
}
