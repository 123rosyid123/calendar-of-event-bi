<?php

namespace Database\Seeders\settings;

use App\Models\SettingLiputanMedia;
use Illuminate\Database\Seeder;

class SettingLiputanMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingLiputanMedia::factory(1)->create();
    }
}
