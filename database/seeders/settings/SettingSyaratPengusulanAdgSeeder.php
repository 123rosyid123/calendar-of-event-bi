<?php

namespace Database\Seeders\settings;

use App\Models\SettingSyaratPengusulanAdg;
use Illuminate\Database\Seeder;

class SettingSyaratPengusulanAdgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingSyaratPengusulanAdg::factory(1)->create();
    }
}
