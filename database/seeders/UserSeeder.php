<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::create([
            'name' => "admin",
            'email' => "admin@email.com",
            'email_verified_at' => now(),
            'password' => bcrypt('admin12345678'),
        ]);
        $kpwbi = User::create([
            'name' => "kpwbi",
            'email' => "kpwbi@email.com",
            'email_verified_at' => now(),
            'password' => bcrypt('kpwbi12345678'),
        ]);
        $satker = User::create([
            'name' => "satker",
            'email' => "satker@email.com",
            'email_verified_at' => now(),
            'password' => bcrypt('satker12345678'),
        ]);
        $dr = User::create([
            'name' => "dr",
            'email' => "dr@email.com",
            'email_verified_at' => now(),
            'password' => bcrypt('dr12345678'),
        ]);

        $admin->assignRole('Admin');
        $kpwbi->assignRole('KPwBI');
        $satker->assignRole('Satker');
        $dr->assignRole('DR');
    }
}
