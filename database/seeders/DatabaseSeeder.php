<?php

namespace Database\Seeders;

use Database\Seeders\settings\SettingBeritaSeeder;
use Database\Seeders\settings\SettingEventPusatSeeder;
use Database\Seeders\settings\SettingFlagshipNasionalSeeder;
use Database\Seeders\settings\SettingKemitraanKolaborasiSeeder;
use Database\Seeders\settings\SettingLiputanMediaSeeder;
use Database\Seeders\settings\SettingPaketSinergiFlagshipSeeder;
use Database\Seeders\settings\SettingPeriodeEventSeeder;
use Database\Seeders\settings\SettingSkalaEventSeeder;
use Database\Seeders\settings\SettingSyaratPengusulanAdgSeeder;
use Database\Seeders\settings\SettingTemaSeeder;
use Database\Seeders\settings\SettingUndanganInternalSeeder;
use Illuminate\Database\Seeder;
use Prophecy\Call\Call;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            ProdukSeeder::class,

            SettingEventPusatSeeder::class,
            SettingFlagshipNasionalSeeder::class,
            SettingKemitraanKolaborasiSeeder::class,
            SettingLiputanMediaSeeder::class,
            SettingPeriodeEventSeeder::class,
            SettingSkalaEventSeeder::class,
            SettingTemaSeeder::class,
            SettingUndanganInternalSeeder::class,
            SettingPaketSinergiFlagshipSeeder::class,
            SettingSyaratPengusulanAdgSeeder::class,
            SettingBeritaSeeder::class,
        ]);
    }
}
