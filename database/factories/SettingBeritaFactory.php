<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SettingBeritaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // dd(User::find(1)->get());
        return [
            'title' => $this->faker->name(),
            'image_name' => 'asdasd.jpg',
            'image_path' => 'news/_01/asdasd.jpg',
            'description' => $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true),
            'user_id' => User::first()->id

        ];
    }
}
