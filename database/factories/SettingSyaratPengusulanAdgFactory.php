<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SettingSyaratPengusulanAdgFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'setting_paket_sinergi_flagship_id' => 1,
            'minimal_jumlah_pengunjung' => rand(1, 10),
            'minimal_nilai_penjualan_exhibition' => rand(1000, 1000000),
            'minimal_nilai_penjualan_ecommerce' => rand(1000, 1000000),
            'minimal_nilai_penjualan_export' => rand(1000, 1000000),
            'minimal_nilai_business_matching_pembiayaan' => rand(1000, 1000000),
            'minimal_nilai_business_matching_export' => rand(1000, 1000000),
            'setting_paket_sinergi_flagship' => 'paket 1',
            'skala_event' => 'nasional,internasional',
        ];
    }
}
