<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SettingKemitraanKolaborasiFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'user_id' => User::whereHas("roles", function ($q) {
            //     $q->where("name", "dr");
            // })->first(),
            'title' => $this->faker->name(),
            'date' => $this->faker->dateTime(),
        ];
    }
}
