<?php

namespace Database\Factories;

use App\Models\Produk;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdukFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    // protected $model = Produk::class;

    public function definition()
    {
        return [
            'nama' => $this->faker->name(),
            'photo' => 'asdasd.jpg',

        ];
    }
}
