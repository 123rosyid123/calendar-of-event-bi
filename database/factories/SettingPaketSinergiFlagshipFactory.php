<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SettingPaketSinergiFlagshipFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'paket '.rand(1,4),
            'detail' => 'detail',
            'date' => $this->faker->dateTime(),
        ];
    }
}
