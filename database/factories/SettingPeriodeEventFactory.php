<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SettingPeriodeEventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tanggal_mulai_pendaftaran' => $this->faker->dateTime(),
            'tanggal_akhir_pendaftaran' => $this->faker->dateTime(),
        ];
    }
}
