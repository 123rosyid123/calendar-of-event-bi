<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventHasFlagshipNasionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_has_flagship_nasional', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id', 'ef_id_foreign')->references('id')->on('events');
            $table->unsignedBigInteger('setting_flagship_nasional_id')->nullable();
            $table->foreign('setting_flagship_nasional_id', 'sf_id_foreign')->references('id')->on('setting_flagship_nasional');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_has_flagship_nasional');
    }
}
