<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingChannelPublikasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_channel_publikasis', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('event_has_channel_publikasi', function (Blueprint $table) {
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id', 'event_cp_id_foreign')->references('id')->on('events');
            $table->unsignedBigInteger('setting_channel_publikasi_id')->nullable();
            $table->foreign('setting_channel_publikasi_id', 'scp_id_foreign')->references('id')->on('setting_channel_publikasis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_channel_publikasis');
        Schema::dropIfExists('event_has_channel_publikasi');
    }
}
