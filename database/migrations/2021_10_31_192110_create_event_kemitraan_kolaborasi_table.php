<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventKemitraanKolaborasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_kemitraan_kolaborasi', function (Blueprint $table) {
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id', 'event_id_foreign')->references('id')->on('events');
            $table->unsignedBigInteger('setting_kemitraan_kolaborasi_id')->nullable();
            $table->foreign('setting_kemitraan_kolaborasi_id', 'sk_id_foreign')->references('id')->on('setting_kemitraan_kolaborasi');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_kemitraan_kolaborasi');
    }
}
