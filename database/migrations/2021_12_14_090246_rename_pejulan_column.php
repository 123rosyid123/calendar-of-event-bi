<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamePejulanColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('nilai_penjulan_ecommerce', 'nilai_penjualan_ecommerce');
            $table->renameColumn('nilai_penjulan_export', 'nilai_penjualan_export');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('nilai_penjualan_ecommerce', 'nilai_penjulan_ecommerce');
            $table->renameColumn('nilai_penjualan_export', 'nilai_penjulan_export');
        });
    }
}
