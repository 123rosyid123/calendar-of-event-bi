<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_user', function (Blueprint $table) {
            // $table->id();
            // $table->timestamps();
            $table->foreignId('user_id')->constrained()->nullable();
            $table->foreignId("produk_id")->constrained('produk')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_user');
    }
}
