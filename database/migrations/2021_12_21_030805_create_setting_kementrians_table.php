<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingKementriansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_kementrians', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('event_has_kementrian', function (Blueprint $table) {
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id', 'event_kementrian_id_foreign')->references('id')->on('events');
            $table->unsignedBigInteger('setting_kementrian_id')->nullable();
            $table->foreign('setting_kementrian_id', 'skem_id_foreign')->references('id')->on('setting_kementrians');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_kementrians');
        Schema::dropIfExists('event_has_kementrian');
    }
}
