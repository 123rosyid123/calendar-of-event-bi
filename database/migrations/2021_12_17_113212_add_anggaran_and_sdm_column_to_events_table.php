<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnggaranAndSdmColumnToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('jumlah_sdm', 'jumlah_sdm_pegawai_organik');

            $table->after('setting_paket_sinergi_flagship_id', function ($table) {
                $table->integer('jumlah_sdm_pegawai_non_organik')->nullable();
                $table->integer('jumlah_sdm_pihak_ketiga')->nullable();
            });

            $table->renameColumn('anggaran_biaya', 'anggaran_biaya_eo');

            // add target percent increase per annual
            $table->after('setting_paket_sinergi_flagship_id', function ($table) {
                $table->bigInteger('anggaran_biaya_publikasi')->nullable();
                $table->bigInteger('anggaran_biaya_logistik_perlengkapan')->nullable();
                $table->bigInteger('anggaran_biaya_honor_pengisi_acara')->nullable();
                $table->bigInteger('anggaran_biaya_lainnya')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('jumlah_sdm_pegawai_organik', 'jumlah_sdm');
            $table->dropColumn('jumlah_sdm_pegawai_non_organik');
            $table->dropColumn('jumlah_sdm_pihak_ketiga');

            $table->renameColumn('anggaran_biaya_eo', 'anggaran_biaya');
            $table->dropColumn('anggaran_biaya_publikasi');
            $table->dropColumn('anggaran_biaya_logistik_perlengkapan');
            $table->dropColumn('anggaran_biaya_honor_pengisi_acara');
            $table->dropColumn('anggaran_biaya_lainnya');
        });
    }
}
