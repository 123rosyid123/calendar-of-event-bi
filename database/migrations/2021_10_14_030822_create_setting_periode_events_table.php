<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingPeriodeEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_periode_event', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('tanggal_mulai_pendaftaran')->nullable();
            $table->dateTime('tanggal_akhir_pendaftaran')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_periode_events');
    }
}
