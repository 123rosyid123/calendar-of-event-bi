<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameEkporColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setting_syarat_pengusulan_adg', function (Blueprint $table) {
            $table->renameColumn('minimal_nilai_businnes_matching_ekspor', 'minimal_nilai_business_matching_export');
            $table->renameColumn('minimal_nilai_businnes_matching_pembiayaan', 'minimal_nilai_business_matching_pembiayaan');

            // add target percent increase per annual
            $table->after('setting_paket_sinergi_flagship_id', function ($table) {
                $table->integer('target_percent_increase');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setting_syarat_pengusulan_adg', function (Blueprint $table) {
            $table->renameColumn('minimal_nilai_business_matching_pembiayaan', 'minimal_nilai_businnes_matching_pembiayaan');
            $table->renameColumn('minimal_nilai_business_matching_export', 'minimal_nilai_businnes_matching_ekspor');

            $table->dropColumn('target_percent_increase');
        });
    }
}
