<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrimaryKeyToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_has_channel_publikasi', function (Blueprint $table) {
            $table->id();
        });

        Schema::table('event_has_iku', function (Blueprint $table) {
            $table->id();
        });

        Schema::table('event_has_kementrian', function (Blueprint $table) {
            $table->id();
        });

        Schema::table('event_kemitraan_kolaborasi', function (Blueprint $table) {
            $table->id();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table', function (Blueprint $table) {
            //
        });
    }
}
