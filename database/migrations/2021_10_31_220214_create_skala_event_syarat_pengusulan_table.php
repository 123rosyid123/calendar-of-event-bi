<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkalaEventSyaratPengusulanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skala_event_syarat_pengusulan', function (Blueprint $table) {
            $table->unsignedBigInteger('skala_event_id')->nullable();
            $table->foreign('skala_event_id', 'se_id_foreign')->references('id')->on('setting_skala_event');
            $table->unsignedBigInteger('syarat_pengusulan_adg_id')->nullable();
            $table->foreign('syarat_pengusulan_adg_id', 'spa_id_foreign')->references('id')->on('setting_syarat_pengusulan_adg');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skala_event_syarat_pengajuan');
    }
}
