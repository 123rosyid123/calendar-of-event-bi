<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlagshipNasionalPaketSinergiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flagship_nasional_paket_sinergi', function (Blueprint $table) {

            $table->unsignedBigInteger('flagship_nasional_id')->nullable();
            $table->foreign('flagship_nasional_id', 'fn_id_foreign')->references('id')->on('setting_flagship_nasional');
            $table->unsignedBigInteger('paket_sinergi_flagship_id')->nullable();
            $table->foreign('paket_sinergi_flagship_id', 'ps_id_foreign')->references('id')->on('setting_paket_sinergi_flagship');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flagship_nasional_paket_sinergi');
    }
}
