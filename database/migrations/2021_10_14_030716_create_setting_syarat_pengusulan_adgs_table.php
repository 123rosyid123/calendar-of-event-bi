<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingSyaratPengusulanAdgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_syarat_pengusulan_adg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('setting_paket_sinergi_flagship_id')->nullable();
            $table->foreign('setting_paket_sinergi_flagship_id', 'paket_sinergi_flagship_id_foreign')->references('id')->on('setting_paket_sinergi_flagship')->onUpdate('cascade')->onDelete('restrict');
            $table->string('minimal_jumlah_pengunjung');
            $table->string('minimal_nilai_penjualan_exhibition');
            $table->string('minimal_nilai_penjualan_ecommerce');
            $table->string('minimal_nilai_penjualan_export');
            $table->string('minimal_nilai_businnes_matching_pembiayaan');
            $table->string('minimal_nilai_businnes_matching_ekspor');
            $table->string('setting_paket_sinergi_flagship');
            $table->text('skala_event');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_syarat_pengusulan_adgs');
    }
}
