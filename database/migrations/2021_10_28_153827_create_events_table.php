<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * available event status :
     *      1 : SUBMITTED
     *      2 : APPROVED
     *      3 : PENDING
     *      4 : REVISED
     *      5 : REJECTED
     *
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->nullable()->constrained();
            $table->integer('event_status')->default(1);

            $table->string('waktu_acara')->nullable();
            $table->string('nama_kpwbi')->nullable();
            $table->string('nama_event')->nullable();
            $table->string('rangkaian_event')->nullable();

            $table->integer('jumlah_pengunjung')->nullable();
            $table->bigInteger('nilai_penjualan_exhibition')->nullable();
            $table->bigInteger('nilai_penjulan_ecommerce')->nullable();
            $table->bigInteger('nilai_penjulan_export')->nullable();
            $table->bigInteger('nilai_business_matching_pembiayaan')->nullable();
            $table->bigInteger('nilai_business_matching_export')->nullable();
            $table->string('onboarding_umkm_baru')->nullable();
            $table->string('perluasan_merchant_qirs')->nullable();

            $table->text('nama_iku')->nullable();
            $table->integer('jumlah_iku')->nullable();
            $table->text('kehadiran_menteri')->nullable();
            $table->text('kehadiran_stakeholder_utama')->nullable();
            $table->text('kehadiran_gubernur')->nullable();
            $table->text('kehadiran_pemimpin_lembaga_pusat')->nullable();
            $table->boolean('is_usulan_kehadiran_adg')->nullable();
            $table->string('usulan_nama_adg')->nullable();
            $table->bigInteger('anggaran_biaya')->nullable();
            $table->integer('jumlah_sdm')->nullable();

            $table->text('channel_publikasi')->nullable();
            $table->string('liputan_media_nasional_lokal')->nullable();
            $table->string('liputan_media_internal')->nullable();
            $table->string('kehadiran_undangan_internal')->nullable();

            $table->string('dokumen_name')->nullable();
            $table->string('dokumen_path')->nullable();

            $table->foreignId('setting_paket_sinergi_flagship_id')->nullable()->constrained("setting_paket_sinergi_flagship");
            $table->foreignId('setting_flagship_nasional_id')->nullable()->constrained("setting_flagship_nasional");
            $table->foreignId('setting_skala_event_id')->nullable()->constrained("setting_skala_event");
            $table->foreignId('setting_tema_id')->nullable()->constrained("setting_tema");
            $table->foreignId('setting_periode_event_id')->nullable()->constrained("setting_periode_event");

            $table->text('temp_update')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
