<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Api\V1\Modul\Dashboard\DashboardController;
use App\Http\Api\V1\Modul\Kurasi\KurasiController;
use App\Http\Api\V1\Modul\Penyesuaian\PenyesuaianController;
use App\Http\Api\V1\Modul\Pendaftaran\PendaftaranController;
use App\Http\Api\V1\Settings\Berita\BeritaController;
use App\Http\Api\V1\Settings\ChannelPublikasi\ChannelPublikasiController;
use App\Http\Api\V1\Settings\Tema\TemaController;
use App\Http\Api\V1\Settings\EventPusat\EventPusatController;
use App\Http\Api\V1\Settings\SkalaEvent\SkalaEventController;
use App\Http\Api\V1\Settings\LiputanMedia\LiputanMediaController;
use App\Http\Api\V1\Settings\PeriodeEvent\PeriodeEventController;
use App\Http\Api\V1\Settings\FlagshipNasional\FlagshipNasionalController;
use App\Http\Api\V1\Settings\Iku\IkuController;
use App\Http\Api\V1\Settings\Kementrian\KementrianController;
use App\Http\Api\V1\Settings\UndanganInternal\UndanganInternalController;
use App\Http\Api\V1\Settings\KemitraanKolaborasi\KemitraanKolaborasiController;
use App\Http\Api\V1\Settings\LegacyEvent\LegacyEventController;
use App\Http\Api\V1\Settings\SyaratPengusulanAdg\SyaratPengusulanAdgController;
use App\Http\Api\V1\Settings\PaketSinergiFlagships\PaketSinergiFlagshipController;
use App\Http\Api\V1\Settings\Produk\ProdukController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\KpwbiController;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::group(['prefix' => 'setting'], function () {
            Route::apiResources([
                'tema' => TemaController::class,
                'event-pusat' => EventPusatController::class,
                'iku' => IkuController::class,
                'kementrian' => KementrianController::class,
                'channel-publikasi' => ChannelPublikasiController::class,
                'skala-event' => SkalaEventController::class,
                'liputan-media' => LiputanMediaController::class,
                'periode-event' => PeriodeEventController::class,
                'flagship-nasional' => FlagshipNasionalController::class,
                'undangan-internal' => UndanganInternalController::class,
                'paket-sinergi' => PaketSinergiFlagshipController::class,
                'pengusulan-adg' => SyaratPengusulanAdgController::class,
                'kemitraan-kolaborasi' => KemitraanKolaborasiController::class,
                'berita' => BeritaController::class,
                'past-event' => LegacyEventController::class,
            ]);
            // Route::get('periode-event/{nama_periode}', [PeriodeEventController::class, 'update']);
        });

        Route::get('event/available-time', [DashboardController::class, 'getAvailableTime']);
        Route::get('event/disabled-date', [DashboardController::class, 'getDisabledDate']);
        Route::apiResource('event', DashboardController::class)
            ->only(['index', 'show']);

        Route::apiResource('setting/kpwbi', KpwbiController::class);
        // ->only(['index', 'show']);

        // modul
        Route::apiResource('pendaftaran', PendaftaranController::class)
            ->only(['index', 'update', 'store', 'destroy', 'show']);
        Route::post('pendaftaran/check_validation', [PendaftaranController::class, 'check_validation']);

        Route::apiResource('kurasi', KurasiController::class)
            ->parameter('kurasi', 'event')
            ->only(['update']);

        Route::apiResource('penyesuaian', PenyesuaianController::class)
            ->parameter('penyesuaian', 'event')
            ->only(['update']);

        Route::apiResource('produk', ProdukController::class);
    });
});

// Auth
Route::apiResource('roles', RoleController::class);
Route::apiResource('users', UserController::class);

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');


Route::get('kirimemail', [MailController::class, 'index']); // TODO : just for dev
