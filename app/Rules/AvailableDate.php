<?php

namespace App\Rules;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AvailableDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = Carbon::parse($value)->format('Y-m-d');
        $time_value = Carbon::parse($value)->format('H:i:s');

        $available_time = [
            "morning" => ['08:00:00', '12:00:00'],
            "afternoon" => ['13:00:00', '17:00:00']
        ];
        $test = Event::whereDate('waktu_acara', '=', $date)->where('event_status', '=', Event::HLE)->get();

        // exist_event diset true untuk mengantisipasi waktu diluar ketentuan
        $exist_event = true;
        foreach ($available_time as $time) {
            if (strtotime($time_value) >= strtotime($time[0]) && strtotime($time_value) <= strtotime($time[1])) {
                $exist_event = Event::whereBetween('waktu_acara', [$date . ' ' . $time[0], $date . ' ' . $time[1]])
                    ->where('event_status', '!=', Event::HLE)
                    ->exists();
            };
        };

        return !$exist_event;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ' :attribute is already used or invalid';
    }
}
