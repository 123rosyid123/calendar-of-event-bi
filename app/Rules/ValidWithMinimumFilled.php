<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Validation\Validator;

class ValidWithMinimumFilled implements Rule
{
    private $parameters = null;
    private $minimum_field = null;
    /**
     * Create a new rule instance.
     * valid value must have filled parameters with minimum
     *
     * @return void
     */
    public function __construct($minimum_field, $params)
    {
        $this->parameters = $params;
        $this->minimum_field = $minimum_field;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        // https://stackoverflow.com/questions/34915547/laravel-validation-or
        // https://stackoverflow.com/questions/58486766/laravel-custom-validation-rule-refering-to-other-request-params/61873671#61873671

        $fields = request()->all(); //data passed to validator

        $existing_data = 0;
        foreach ($this->parameters as $param) {

            $existValue = Arr::get($fields, $param, false);

            if ($existValue) { //if exist value is present validation not passed
                $existing_data++;
            }
        }
        if ($existing_data < $this->minimum_field) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $fields = implode(', ', $this->parameters);
        return "The :attribute field cannot be filled unless $fields is filled in at least $this->minimum_field fields";
    }
}
