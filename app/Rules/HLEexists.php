<?php

namespace App\Rules;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class HLEexists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = Carbon::parse(request()->waktu_acara)->format('Y-m-d');
        $exist_event = event::whereDate('waktu_acara', '=', $date)->where('event_status', '=', Event::HLE)->exists();

        return $value ? !$exist_event : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'cannot invite ADG while HLE event is exists.';
    }
}
