<?php

namespace App\Rules;

use App\Models\Event;
use App\Models\SettingSyaratPengusulanAdg;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class EventTargetMinimumValue implements Rule, DataAwareRule
{

    private $percent_increase;
    private $minimum_value;
    protected $data = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public function __construct()
    {
        // $this->fields = 
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */

    /**
     * Set the data under validation.
     *
     * @param  array  $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function passes($attribute, $value)
    {
        $sum_fields = array_filter(
            $this->data,
            fn ($key) => in_array($key, [
                'nilai_penjualan_exhibition', 'nilai_penjualan_ecommerce',
                'nilai_penjualan_export', 'nilai_business_matching_pembiayaan',
                'nilai_business_matching_export',
            ]),
            ARRAY_FILTER_USE_KEY
        );

        $target_sum = array_sum(array_diff_key($sum_fields, array_flip(array('nama_event', 'kpwbi_id'))));

        $minimum_requirement = SettingSyaratPengusulanAdg::select(DB::raw(
            'IFNULL(minimal_nilai_penjualan_exhibition,0) 
                + IFNULL(minimal_nilai_penjualan_ecommerce,0) 
                + IFNULL(minimal_nilai_penjualan_export,0) 
                + IFNULL(minimal_nilai_business_matching_pembiayaan,0) 
                + IFNULL(minimal_nilai_business_matching_export,0) 
                as target_sum'
        ), 'target_percent_increase')->latest()->first();

        $past_event = Event::select(DB::raw(
            'IFNULL(nilai_penjualan_exhibition,0) 
                + IFNULL(nilai_penjualan_ecommerce,0) 
                + IFNULL(nilai_penjualan_export,0) 
                + IFNULL(nilai_business_matching_pembiayaan,0) 
                + IFNULL(nilai_business_matching_export,0) 
                as target_sum'
        ))
            ->where('nama_event', $this->data['nama_event'])
            ->where('kpwbi_id', $this->data['kpwbi_id'])
            ->orderBy('id', 'DESC')
            ->first();

        $this->percent_increase = $minimum_requirement->target_percent_increase; // disetting berdasarkan database
        $minimum_value = $past_event ?
            $past_event->target_sum :
            $minimum_requirement->target_sum;

        $this->minimum_value = $minimum_value * (100 + $this->percent_increase) / 100;

        return ($target_sum >= $this->minimum_value); // kenaikan harga adalah x persen dari jumlah target tahun sebelumnya
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Total target tidak mencapai nilai minimum, nilai minimum adalah $this->minimum_value.";
    }
}
