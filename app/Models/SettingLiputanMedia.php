<?php

namespace App\Models;

use Carbon\Carbon;

class SettingLiputanMedia extends BaseModel
{
    protected $table = 'setting_liputan_media';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'title',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->date = Carbon::now()->toDateTimeString();
        });
    }
}
