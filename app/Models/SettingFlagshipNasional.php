<?php

namespace App\Models;

use Carbon\Carbon;

class SettingFlagshipNasional extends BaseModel
{
    protected $table = 'setting_flagship_nasional';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'title',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->date = Carbon::now()->toDateTimeString();
        });
    }
}
