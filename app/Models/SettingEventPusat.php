<?php

namespace App\Models;

class SettingEventPusat extends BaseModel
{
    protected $table = 'setting_event_pusat';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'event_id',
        'name',
        'deskripsi',
        'date',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function event()
    {
        return $this->BelongsTo(Event::class);
    }
}
