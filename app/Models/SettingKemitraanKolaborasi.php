<?php

namespace App\Models;

use Carbon\Carbon;

class SettingKemitraanKolaborasi extends BaseModel
{
    protected $table = 'setting_kemitraan_kolaborasi';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'created_by',
        'title',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->date = Carbon::now()->toDateTimeString();
        });
    }
}
