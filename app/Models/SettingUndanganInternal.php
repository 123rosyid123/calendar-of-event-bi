<?php

namespace App\Models;

use Carbon\Carbon;

class SettingUndanganInternal extends BaseModel
{
    protected $table = 'setting_undangan_internal';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'title',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->date = Carbon::now()->toDateTimeString();
        });
    }
}
