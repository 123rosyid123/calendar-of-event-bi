<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingBerita extends BaseModel
{
    use HasFactory;
    protected $table = 'setting_berita';
    protected $fillable = [
        'title', 'description', 'image_name', 'image_path', 'user_id'
    ];

    public function create_by()
    {
        return $this->belongsTo(User::class);
    }
}
