<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;
    protected $table = 'produk';
    protected $fillable = [
        'nama', 'photo'
    ];


    public function users()
    {
        return $this->belongsToMany(User::class, "produk_user", "produk_id", "user_id");
    }

    public function syncUsers($ids)
    {
        $existing_ids = $this->users()->whereIn('users.id', $ids)->pluck('users.id');
        $remove_ids = $this->users()->whereNotIn('users.id', $ids)->pluck('users.id');

        $this->users()->attach(collect($ids)->diff($existing_ids));
        $this->users()->detach($remove_ids);
    }
}
