<?php

namespace App\Models;

use Carbon\Carbon;

class SettingPaketSinergiFlagship extends BaseModel
{
    protected $table = 'setting_paket_sinergi_flagship';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'title',
        'detail',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function getDetailFlagship()
    {
        return null;
    }

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->date = Carbon::now()->toDateTimeString();
        });

        static::updated(function ($model) {
            // dd([1, 2, 3]);
            // dd($model->flagship_nasional()->pluck('title'));
            // $model->detail =  $model->flagship_nasional()->implode('title', ', ');
            // dd($model);
            // $model->save();
        });
    }

    public function flagship_nasional()
    {
        return $this->belongsToMany(SettingFlagshipNasional::class, "flagship_nasional_paket_sinergi", "paket_sinergi_flagship_id", "flagship_nasional_id");
    }

    public function syncFlagshipNasional($ids)
    {
        $existing_ids = $this->flagship_nasional()->whereIn('setting_flagship_nasional.id', $ids)->pluck('setting_flagship_nasional.id');
        $remove_ids = $this->flagship_nasional()->whereNotIn('setting_flagship_nasional.id', $ids)->pluck('setting_flagship_nasional.id');

        $this->flagship_nasional()->attach(collect($ids)->diff($existing_ids));
        $this->flagship_nasional()->detach($remove_ids);
    }
}
