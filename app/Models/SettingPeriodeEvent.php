<?php

namespace App\Models;

class SettingPeriodeEvent extends BaseModel
{
    protected $table = 'setting_periode_event';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'tanggal_mulai_pendaftaran',
        'tanggal_akhir_pendaftaran',
        'nama_periode',
        'is_active',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function getRouteKeyName()
    {
        return 'nama_periode';
    }

    protected static function boot()
    {
        parent::boot();
    }
}
