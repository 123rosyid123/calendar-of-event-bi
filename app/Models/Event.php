<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends BaseModel
{
    use HasFactory;

    const SUBMITTED = 1;
    const APPROVED = 2;
    const EVALUATED = 3;
    const REVISED = 4;
    const REJECTED = 5;
    const HLE = 10;
    const LEGACY_EVENT = 11;

    const status_event = [
        1 => 'SUBMITTED',
        2 => 'APPROVED',
        3 => 'EVALUATED',
        4 => 'REVISED',
        5 => 'REJECTED',
        10 => 'HLE',
        11 => 'LEGACY EVENT'
    ];

    protected $fillable = [
        'user_id',
        'kpwbi_id',

        'event_status',
        'waktu_acara',
        'nama_kpwbi',
        'nama_event',
        'rangkaian_event',
        'jumlah_pengunjung',
        'nilai_penjualan_exhibition',
        'nilai_penjualan_ecommerce',
        'nilai_penjualan_export',
        'nilai_business_matching_pembiayaan',
        'nilai_business_matching_export',
        'onboarding_umkm_baru',
        'perluasan_merchant_qirs',
        'nama_iku',
        'jumlah_iku',
        'kehadiran_menteri',
        'kehadiran_stakeholder_utama',
        'kehadiran_gubernur',
        'kehadiran_pemimpin_lembaga_pusat',
        'is_usulan_kehadiran_adg',
        'usulan_nama_adg',
        'anggaran_biaya_eo',
        'anggaran_biaya_publikasi',
        'anggaran_biaya_logistik_perlengkapan',
        'anggaran_biaya_honor_pengisi_acara',
        'anggaran_biaya_lainnya',
        'jumlah_sdm_pegawai_organik',
        'jumlah_sdm_pegawai_non_organik',
        'jumlah_sdm_pihak_ketiga',
        'channel_publikasi',
        'liputan_media_nasional_lokal',
        'liputan_media_internal',
        'kehadiran_undangan_internal',
        'paket_sinergi_flagship_id',
        'flagship_nasional_id',
        'skala_event_id',
        'tema_id',
        'dokumen_name',
        'dokumen_path',
        'dokumen_pdf_name',
        'dokumen_pdf_path',

        'temp_update',

        'setting_paket_sinergi_flagship_id',
        'setting_flagship_nasional_id',
        'setting_skala_event_id',
        'setting_tema_id',

        'setting_periode_event_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function kpwbi()
    {
        return $this->belongsTo(Kpwbi::class);
    }

    public function paket_sinergi_flagship()
    {
        return $this->belongsTo(SettingPaketSinergiFlagship::class, 'setting_paket_sinergi_flagship_id');
    }


    public function tema()
    {
        return $this->belongsTo(SettingTema::class, 'setting_tema_id');
    }

    public function flagship_nasional()
    {
        return $this->belongsTo(SettingFlagshipNasional::class, 'setting_flagship_nasional_id');
    }

    public function setting_kemitraan_kolaborasi()
    {
        return $this->belongsToMany(SettingKemitraanKolaborasi::class, "event_kemitraan_kolaborasi", "event_id", "setting_kemitraan_kolaborasi_id");
    }

    public function setting_iku()
    {
        return $this->belongsToMany(SettingIku::class, "event_has_iku", "event_id", "setting_iku_id",);
    }

    public function sync_kemitraan_kolaborasi($ids)
    {
        $this->sync($ids, $this->setting_kemitraan_kolaborasi(), 'setting_kemitraan_kolaborasi.id');
    }

    public function sync_iku($ids)
    {
        $this->sync($ids, $this->setting_iku(), 'setting_ikus.id');
    }

    public function setting_kementrian()
    {
        return $this->belongsToMany(SettingKementrian::class, "event_has_kementrian", "event_id", "setting_kementrian_id",);
    }

    public function sync_kementrian($ids)
    {
        $this->sync($ids, $this->setting_kementrian(), 'setting_kementrians.id');
    }

    public function setting_channel_publikasi()
    {
        return $this->belongsToMany(SettingChannelPublikasi::class, "event_has_channel_publikasi", "event_id", "setting_channel_publikasi_id",);
    }

    public function sync_channel_publikasi($ids)
    {
        $this->sync($ids, $this->setting_channel_publikasi(), 'setting_channel_publikasis.id');
    }

    public function setting_tema()
    {
        return $this->belongsToMany(SettingTema::class, "event_has_tema", "event_id", "setting_tema_id",);
    }

    public function sync_tema($ids)
    {
        $this->sync($ids, $this->setting_tema(), 'setting_tema.id');
    }

    public function setting_flagship_nasional()
    {
        return $this->belongsToMany(SettingFlagshipNasional::class, "event_has_flagship_nasional", "event_id", "setting_flagship_nasional_id",);
    }

    public function sync_flagship_nasional($ids)
    {
        $this->sync($ids, $this->setting_flagship_nasional(), 'setting_flagship_nasional.id');
    }

    public function komentars()
    {
        return $this->hasMany(Komentar::class);
    }

    private function sync($list_id, $relation, $field)
    {
        $existing_ids = $relation->whereIn($field, $list_id)->pluck($field);
        $remove_ids = $relation->whereNotIn($field, $list_id)->pluck($field);

        $relation->attach(collect($list_id)->diff($existing_ids));
        $relation->detach($remove_ids);
    }
}
