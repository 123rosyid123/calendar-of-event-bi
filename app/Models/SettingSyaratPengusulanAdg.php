<?php

namespace App\Models;

class SettingSyaratPengusulanAdg extends BaseModel
{
    protected $table = 'setting_syarat_pengusulan_adg';
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'setting_paket_sinergi_flagship_id',
        'minimal_jumlah_pengunjung',
        'minimal_nilai_penjualan_exhibition',
        'minimal_nilai_penjualan_ecommerce',
        'minimal_nilai_penjualan_export',
        'minimal_nilai_business_matching_pembiayaan',
        'minimal_nilai_business_matching_export',
        'setting_paket_sinergi_flagship',
        'target_percent_increase',
        'skala_event',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function paket_sinergi_flagship()
    {
        return $this->belongsTo(SettingPaketSinergiFlagship::class, 'setting_paket_sinergi_flagship_id');
    }

    public function skala_events()
    {
        return $this->belongsToMany(SettingSkalaEvent::class, "skala_event_syarat_pengusulan", "syarat_pengusulan_adg_id", "skala_event_id");
    }

    public function sync_skala_event($ids)
    {
        $existing_ids = $this->skala_events()->whereIn('setting_skala_event.id', $ids)->pluck('setting_skala_event.id');
        $remove_ids = $this->skala_events()->whereNotIn('setting_skala_event.id', $ids)->pluck('setting_skala_event.id');

        $this->skala_events()->attach(collect($ids)->diff($existing_ids));
        $this->skala_events()->detach($remove_ids);
    }

    protected static function boot()
    {
        parent::boot();
    }
}
