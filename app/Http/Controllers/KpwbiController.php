<?php

namespace App\Http\Controllers;

use App\Http\Requests\KpwbiRequestCreate;
use App\Http\Requests\KpwbiRequestUpdate;
use App\Http\Resources\EventResource;
use App\Http\Resources\KpwbiResource;
use App\Http\Resources\UserResource;
use App\Models\Kpwbi;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class KpwbiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $kpwbis = Kpwbi::with('users', 'events');
        $action = $per_page == -1
            ? $kpwbis->get()
            : $kpwbis->paginate($per_page);

        abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return KpwbiResource::collection($action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KpwbiRequestCreate $request)
    {
        $action = Kpwbi::create($request->validated());
        return (new KpwbiResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kpwbi $kpwbi)
    {
        return new KpwbiResource($kpwbi);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KpwbiRequestUpdate $request, Kpwbi $kpwbi)
    {
        $kpwbi->updateOrFail($request->validated());
        return new KpwbiResource($kpwbi);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kpwbi $kpwbi)
    {
        abort_if(
            $kpwbi->events()->exists() || $kpwbi->users()->exists(),
            response([
                'message' => 'kpwbi sudah digunakan',
                'events' => EventResource::collection($kpwbi->events),
                'users' => UserResource::collection($kpwbi->users)
            ], Response::HTTP_NOT_ACCEPTABLE)
        );
        $kpwbi->delete();
        return response(['message' => 'Success delete']);
    }
}
