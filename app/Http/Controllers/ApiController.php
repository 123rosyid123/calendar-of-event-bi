<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\SoftDeletes;

class ApiController extends Controller
{
    use SoftDeletes;
}
