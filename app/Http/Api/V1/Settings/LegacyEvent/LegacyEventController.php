<?php

namespace App\Http\Api\V1\Settings\LegacyEvent;

use App\Http\Api\V1\Settings\LegacyEvent\Requests\LegacyEventRequest;
use App\Http\Controllers\ApiController;
use App\Http\Resources\EventResource;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class LegacyEventController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $events = Event::where('event_status', Event::LEGACY_EVENT);
        $action = $per_page == -1
            ? $events->get()
            : $events->paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return EventResource::collection($action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LegacyEventRequest $request)
    {
        $data = $request->safe()->merge([
            'setting_paket_sinergi_flagship_id' => $request->paket_sinergi_flagship_id,
            'setting_flagship_nasional_id' => $request->flagship_nasional_id,
            'setting_skala_event_id' => $request->skala_event_id,
            'setting_tema_id' => $request->tema_id,
            'event_status' => Event::LEGACY_EVENT,
            'user_id' => Auth::user()->id,
        ])->except([
            'paket_sinergi_flagship_id',
            'flagship_nasional_id',
            'skala_event_id',
            'tema_id'
        ]);

        $event = Event::create($data);
        return (new EventResource($event))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $past_event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $past_event)
    {
        return new EventResource($past_event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $past_event
     * @return \Illuminate\Http\Response
     */
    public function update(LegacyEventRequest $request, Event $past_event)
    {
        $data = $request->safe()->merge([
            'setting_paket_sinergi_flagship_id' => $request->paket_sinergi_flagship_id,
            'setting_flagship_nasional_id' => $request->flagship_nasional_id,
            'setting_skala_event_id' => $request->skala_event_id,
            'setting_tema_id' => $request->tema_id,
        ])->except([
            'paket_sinergi_flagship_id',
            'flagship_nasional_id',
            'skala_event_id',
            'tema_id'
        ]);
        $past_event->update($data);
        return new EventResource($past_event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $past_event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $past_event)
    {
        $past_event->delete();
        return response(['message' => 'Success delete']);
    }
}
