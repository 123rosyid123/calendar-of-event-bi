<?php

namespace App\Http\Api\V1\Settings\Produk\Resources;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProdukResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        // dd($this->whenLoaded('users')->isMising());
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            // 'authors' => $this->whenLoaded('users') ? $this->whenLoaded('users')->implode('name',', ') : '',
            'users' => UserResource::collection($this->whenLoaded('users')),
        ];
    }
}
