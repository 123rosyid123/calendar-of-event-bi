<?php

namespace App\Http\Api\V1\Settings\LegacyEvent\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LegacyEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'waktu_acara' => ['sometimes', 'required', 'date'],

            'nama_event' => 'required',
            'rangkaian_event' => 'sometimes',
            'jumlah_pengunjung' => "numeric",
            'nilai_penjualan_exhibition' => "required|numeric",
            'nilai_penjualan_ecommerce' => "required|numeric",
            'nilai_penjualan_export' => "required|numeric",
            'nilai_business_matching_pembiayaan' => "required|numeric",
            'nilai_business_matching_export' => "required|numeric",
            'onboarding_umkm_baru' => 'sometimes',
            'perluasan_merchant_qirs' => 'sometimes',

            'paket_sinergi_flagship_id' => 'sometimes|exists:App\Models\SettingPaketSinergiFlagship,id',
            'flagship_nasional_id' => 'sometimes|exists:App\Models\SettingFlagshipNasional,id',
            'skala_event_id' => 'sometimes|exists:App\Models\SettingSkalaEvent,id',
            'tema_id' => 'sometimes|exists:App\Models\SettingTema,id',
            'kpwbi_id' => 'required|exists:App\Models\Kpwbi,id',

        ];
    }
}
