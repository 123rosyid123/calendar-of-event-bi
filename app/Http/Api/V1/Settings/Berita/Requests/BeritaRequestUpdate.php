<?php

namespace App\Http\Api\V1\Settings\Berita\Requests;


use Illuminate\Foundation\Http\FormRequest;

class BeritaRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'sometimes|required',
            'description' => 'sometimes|required',
            'image' => 'sometimes|file|mimes:jpeg,jpg,png,gif|max:10000' // max 10000 kb
        ];
    }
}
