<?php

namespace App\Http\Api\V1\Settings\Berita;

use App\Http\Api\V1\Settings\Berita\Requests\BeritaRequestCreate;
use App\Http\Api\V1\Settings\Berita\Requests\BeritaRequestUpdate;
use App\Http\Api\V1\Settings\Berita\Resources\BeritaResource;
use App\Http\Controllers\ApiController;
use App\Models\SettingBerita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class BeritaController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingBerita::all()
            : SettingBerita::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return BeritaResource::collection($action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BeritaRequestCreate $request)
    {
        $validated = $request->safe()->merge([
            'user_id' => Auth::user()->id
        ]);
        $action = SettingBerita::create($validated->all());

        // upload document
        if ($request->hasFile('image')) {
            $image_name = $request->file('image')->getClientOriginalName();
            $file_name = sha1(rand()) . '_' . str_replace(' ', '_', $image_name);
            $image_path = request()->file('image')->storeAs('news', '_' . $request->user()->id . '/' . $file_name, 'public_storage');
            $action->update([
                'image_name' => $image_name,
                'image_path' => $image_path
            ]);
        }

        return (new BeritaResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SettingBerita  $beritum
     * @return \Illuminate\Http\Response
     */
    public function show(SettingBerita $beritum)
    {
        return new BeritaResource($beritum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SettingBerita  $beritum
     * @return \Illuminate\Http\Response
     */
    public function update(BeritaRequestUpdate $request, SettingBerita $beritum)
    {
        $beritum->update($request->validated());

        // upload document
        if ($request->hasFile('image')) {
            $image_name = $request->file('image')->getClientOriginalName();
            $file_name = sha1(rand()) . '_' . str_replace(' ', '_', $image_name);
            $image_path = request()->file('image')->storeAs('news', '_' . ($request->user()->id ?? 'anonim') . '/' . $file_name, 'public_storage');

            Storage::delete($beritum->image_path);
            $beritum->update([
                'image_name' => $image_name,
                'image_path' => $image_path
            ]);
        }
        return new BeritaResource($beritum);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SettingBerita  $beritum
     * @return \Illuminate\Http\Response
     */
    public function destroy(SettingBerita $beritum)
    {
        $beritum->delete();
        return response(['message' => 'Success delete']);
    }
}
