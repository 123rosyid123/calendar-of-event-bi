<?php

namespace App\Http\Api\V1\Settings\Produk\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdukRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "nama" => "required",
            "photo" => "required|file"
        ];
    }
}
