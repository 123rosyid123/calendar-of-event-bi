<?php

namespace App\Http\Api\V1\Settings\Produk;

use App\Http\Api\V1\Settings\Produk\Requests\ProdukRequest;
use App\Http\Api\V1\Settings\Produk\Requests\ProdukRequestCreate;
use App\Http\Api\V1\Settings\Produk\Resources\ProdukResource;
use App\Http\Controllers\ApiController;
use App\Models\Produk;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class ProdukController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Auth::user()->tokens());
        $waw = Produk::find(1);
        $id_user = array_unique([1]);
        $waw->syncUsers($id_user);
        $produk = Produk::with('users')->get();

        return ProdukResource::collection($produk);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdukRequestCreate $request)
    {
        // $produk = new Produk;
        $produk = Produk::create($request->all());
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo')->getClientOriginalName();
            $file_path = request()->file('photo')->storeAs('wadaw', 'asd/' . $photo, 'public_storage');
            dd($file_path);
            $produk->update(['photo' => $photo]);
        }

        return (new ProdukResource($produk))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
