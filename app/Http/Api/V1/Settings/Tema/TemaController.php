<?php

namespace App\Http\Api\V1\Settings\Tema;

use App\Http\Api\V1\Settings\Tema\Requests\TemaRequestCreate;
use App\Http\Api\V1\Settings\Tema\Requests\TemaRequestUpdate;
use App\Http\Api\V1\Settings\Tema\Resources\TemaCollection;
use App\Http\Api\V1\Settings\Tema\Resources\TemaResource;
use App\Models\SettingTema;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpFoundation\Response;

class TemaController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingTema::all()
            : SettingTema::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return TemaResource::collection($action);
    }

    public function store(TemaRequestCreate $request)
    {
        $action = SettingTema::create($request->all());
        return (new TemaResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingTema $tema)
    {
        return new TemaResource($tema);
    }

    public function update(TemaRequestUpdate $request, SettingTema $tema)
    {
        $tema->updateOrFail($request->all());
        return new TemaResource($tema);
    }

    public function destroy(SettingTema $tema)
    {
        $tema->delete();
        return response(['message' => 'Success delete']);
    }
}
