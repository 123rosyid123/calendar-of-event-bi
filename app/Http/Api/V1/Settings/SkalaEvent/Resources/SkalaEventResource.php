<?php

namespace App\Http\Api\V1\Settings\SkalaEvent\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SkalaEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            "date" => $this->date
        ];
    }
}
