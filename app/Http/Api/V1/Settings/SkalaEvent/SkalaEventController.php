<?php

namespace App\Http\Api\V1\Settings\SkalaEvent;

use App\Http\Api\V1\Settings\SkalaEvent\Requests\SkalaEventRequestCreate;
use App\Http\Api\V1\Settings\SkalaEvent\Requests\SkalaEventRequestUpdate;
use App\Http\Api\V1\Settings\SkalaEvent\Resources\SkalaEventCollection;
use App\Http\Api\V1\Settings\SkalaEvent\Resources\SkalaEventResource;
use Illuminate\Http\Request;
use App\Models\SettingSkalaEvent;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpFoundation\Response;

class SkalaEventController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingSkalaEvent::all()
            : SettingSkalaEvent::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return new SkalaEventCollection($action);
    }

    public function store(SkalaEventRequestCreate $request)
    {
        $action = SettingSkalaEvent::create($request->all());
        return (new SkalaEventResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingSkalaEvent $skala_event)
    {
        return new SkalaEventResource($skala_event);
    }

    public function update(SkalaEventRequestUpdate $request, SettingSkalaEvent $skala_event)
    {
        $skala_event->updateOrFail($request->all());
        return new SkalaEventResource($skala_event);
    }

    public function destroy(SettingSkalaEvent $skala_event)
    {
        $skala_event->delete();
        return response(['message' => 'Success delete']);
    }
}
