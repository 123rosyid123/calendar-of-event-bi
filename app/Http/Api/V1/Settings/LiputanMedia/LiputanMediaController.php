<?php

namespace App\Http\Api\V1\Settings\LiputanMedia;

use App\Http\Api\V1\Settings\LiputanMedia\Requests\LiputanMediaRequestCreate;
use App\Http\Api\V1\Settings\LiputanMedia\Requests\LiputanMediaRequestUpdate;
use App\Http\Api\V1\Settings\LiputanMedia\Resources\LiputanMediaCollection;
use App\Http\Api\V1\Settings\LiputanMedia\Resources\LiputanMediaResource;
use Illuminate\Http\Request;
use App\Models\SettingLiputanMedia;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpFoundation\Response;

class LiputanMediaController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingLiputanMedia::all()
            : SettingLiputanMedia::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return new LiputanMediaCollection($action);
    }

    public function store(LiputanMediaRequestCreate $request)
    {
        $action = SettingLiputanMedia::create($request->validated());
        return (new LiputanMediaResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingLiputanMedia $liputan_media)
    {
        return new LiputanMediaResource($liputan_media);
    }

    public function update(LiputanMediaRequestUpdate $request, SettingLiputanMedia $liputan_medium)
    {
        // dd($liputan_medium);
        $liputan_medium->updateOrFail($request->validated());
        return new LiputanMediaResource($liputan_medium);
    }

    public function destroy(SettingLiputanMedia $liputan_medium)
    {
        $liputan_medium->delete();
        return response(['message' => 'Success delete']);
    }
}
