<?php

namespace App\Http\Api\V1\Settings\LiputanMedia\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LiputanMediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
