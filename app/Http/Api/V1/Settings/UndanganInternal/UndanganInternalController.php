<?php

namespace App\Http\Api\V1\Settings\UndanganInternal;

use App\Http\Api\V1\Settings\UndanganInternal\Requests\UndanganinternalRequestCreate;
use App\Http\Api\V1\Settings\UndanganInternal\Requests\UndanganinternalRequestUpdate;
use App\Http\Api\V1\Settings\UndanganInternal\Resources\UndanganInternalCollection;
use App\Http\Api\V1\Settings\UndanganInternal\Resources\UndanganInternalResoure;
use Illuminate\Http\Request;
use App\Models\SettingUndanganInternal;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpFoundation\Response;

class UndanganInternalController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingUndanganInternal::all()
            : SettingUndanganInternal::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return UndanganInternalResoure::collection($action);
    }

    public function store(UndanganinternalRequestCreate $request)
    {
        $action = SettingUndanganInternal::create($request->validated());
        return (new UndanganInternalResoure($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingUndanganInternal $undangan_internal)
    {
        return new UndanganInternalResoure($undangan_internal);
    }

    public function update(UndanganinternalRequestUpdate $request, SettingUndanganInternal $undangan_internal)
    {
        $undangan_internal->updateOrFail($request->validated());
        return new UndanganInternalResoure($undangan_internal);
    }

    public function destroy(SettingUndanganInternal $undangan_internal)
    {
        $undangan_internal->delete();
        return response(['message' => 'Success delete']);
    }
}
