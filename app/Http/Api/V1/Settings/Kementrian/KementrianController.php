<?php

namespace App\Http\Api\V1\Settings\Kementrian;

use App\Http\Api\V1\Settings\Kementrian\Requests\KementrianRequestCreate;
use App\Http\Api\V1\Settings\Kementrian\Requests\KementrianRequestUpdate;
use App\Http\Api\V1\Settings\Kementrian\Resources\KementrianCollection;
use App\Http\Api\V1\Settings\Kementrian\Resources\KementrianResource;
use App\Models\SettingKementrian;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpFoundation\Response;

class KementrianController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingKementrian::all()
            : SettingKementrian::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return KementrianResource::collection($action);
    }

    public function store(KementrianRequestCreate $request)
    {
        $action = SettingKementrian::create($request->validated());
        return (new KementrianResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingKementrian $kementrian)
    {
        return new KementrianResource($kementrian);
    }

    public function update(KementrianRequestUpdate $request, SettingKementrian $kementrian)
    {
        $kementrian->updateOrFail($request->all());
        return new KementrianResource($kementrian);
    }

    public function destroy(SettingKementrian $kementrian)
    {
        $kementrian->delete();
        return response(['message' => 'Success delete']);
    }
}
