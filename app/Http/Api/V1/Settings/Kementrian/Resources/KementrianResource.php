<?php

namespace App\Http\Api\V1\Settings\Kementrian\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class KementrianResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            "date" => Carbon::parse($this->created_at)->format('Y-m-d H:m:s'),
        ];
    }
}
