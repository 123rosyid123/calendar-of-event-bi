<?php

namespace App\Http\Api\V1\Settings\SyaratPengusulanAdg\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SyaratPengusulanAdgRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'minimal_jumlah_pengunjung' => "required",
            'minimal_nilai_penjualan_exhibition' => "required",
            'minimal_nilai_penjualan_ecommerce' => "required",
            'minimal_nilai_penjualan_export' => "required",
            'minimal_nilai_business_matching_pembiayaan' => "required",
            'minimal_nilai_business_matching_export' => "required",
            'setting_paket_sinergi_flagship' => "sometimes",
            'skala_event' => 'sometimes',
            'target_percent_increase' => 'required',

            'setting_paket_sinergi_flagship_id' => "required",
            'skala_event_ids' => 'required|array',

        ];
    }
}
