<?php

namespace App\Http\Api\V1\Settings\SyaratPengusulanAdg\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SyaratPengusulanAdgRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'minimal_jumlah_pengunjung' => "sometimes|required",
            'minimal_nilai_penjualan_exhibition' => "sometimes|required",
            'minimal_nilai_penjualan_ecommerce' => "sometimes|required",
            'minimal_nilai_penjualan_export' => "sometimes|required",
            'minimal_nilai_business_matching_pembiayaan' => "sometimes|required",
            'minimal_nilai_business_matching_export' => "sometimes|required",
            'setting_paket_sinergi_flagship' => "sometimes",
            'skala_event' => 'sometimes',

            'setting_paket_sinergi_flagship_id' => "sometimes|required",
            'skala_event_ids' => 'sometimes|required|array',
        ];
    }
}
