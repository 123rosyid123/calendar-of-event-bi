<?php

namespace App\Http\Api\V1\Settings\SyaratPengusulanAdg;

use App\Http\Api\V1\Settings\SyaratPengusulanAdg\Requests\SyaratPengusulanAdgRequestCreate;
use App\Http\Api\V1\Settings\SyaratPengusulanAdg\Requests\SyaratPengusulanAdgRequestUpdate;
use App\Http\Api\V1\Settings\SyaratPengusulanAdg\Resources\SyaratPengusulanAdgResource;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\SettingSyaratPengusulanAdg;
use Illuminate\Auth\Events\Validated;
use Symfony\Component\HttpFoundation\Response;

class SyaratPengusulanAdgController extends ApiController
{
    public function index()
    {
        // $action = SettingSyaratPengusulanAdg::with('skala_events', 'paket_sinergi_flagship')->paginate(5);
        $action = SettingSyaratPengusulanAdg::with('skala_events', 'paket_sinergi_flagship')->latest()->first();
        // dd($action);
        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return new SyaratPengusulanAdgResource($action);
    }

    public function store(SyaratPengusulanAdgRequestCreate $request)
    {
        $Validated = $request->validated();
        $action = SettingSyaratPengusulanAdg::create($Validated);
        $action->sync_skala_event(array_unique($Validated['skala_event_ids']));
        $action->load('paket_sinergi_flagship');

        return (new SyaratPengusulanAdgResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingSyaratPengusulanAdg $pengusulan_adg)
    {
        return new SyaratPengusulanAdgResource($pengusulan_adg);
    }

    public function update(SyaratPengusulanAdgRequestUpdate $request, SettingSyaratPengusulanAdg $pengusulan_adg)
    {
        // dd($request->validated());
        $pengusulan_adg->updateOrFail($request->validated());
        $pengusulan_adg->sync_skala_event(array_unique($request->validated()['skala_event_ids']));
        $pengusulan_adg->load('paket_sinergi_flagship');
        return new SyaratPengusulanAdgResource($pengusulan_adg);
    }

    public function destroy(SettingSyaratPengusulanAdg $pengusulan_adg)
    {
        $pengusulan_adg->delete();
        return response(['message' => 'Success delete']);
    }
}
