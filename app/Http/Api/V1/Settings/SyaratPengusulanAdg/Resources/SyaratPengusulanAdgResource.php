<?php

namespace App\Http\Api\V1\Settings\SyaratPengusulanAdg\Resources;

use App\Http\Api\V1\Settings\SkalaEvent\Resources\SkalaEventResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SyaratPengusulanAdgResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // dd($this);
        return [
            "id" => $this->id,
            "setting_paket_sinergi_flagship_id" => $this->setting_paket_sinergi_flagship_id,
            "minimal_jumlah_pengunjung" => $this->minimal_jumlah_pengunjung,
            "minimal_nilai_penjualan_exhibition" => $this->minimal_nilai_penjualan_exhibition,
            "minimal_nilai_penjualan_ecommerce" => $this->minimal_nilai_penjualan_ecommerce,
            "minimal_nilai_penjualan_export" => $this->minimal_nilai_penjualan_export,
            "minimal_nilai_business_matching_pembiayaan" => $this->minimal_nilai_business_matching_pembiayaan,
            "minimal_nilai_business_matching_export" => $this->minimal_nilai_business_matching_export,
            "target_percent_increase" => $this->target_percent_increase,

            "paket_sinergi_flagship" => $this->whenLoaded('paket_sinergi_flagship')
                ? $this->whenLoaded('paket_sinergi_flagship')->title
                : null,
            // "paket_sinergi_flagship" => $this->paket_sinergi_flagship->title,
            'skala_event' => $this->skala_events->implode('title', ', '),
            "skala_event_details" => SkalaEventResource::collection(($this->whenLoaded('skala_events')))
        ];
    }
}
