<?php

namespace App\Http\Api\V1\Settings\Iku\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IkuRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'sometimes',
            'detail' => 'sometimes'
        ];
    }
}
