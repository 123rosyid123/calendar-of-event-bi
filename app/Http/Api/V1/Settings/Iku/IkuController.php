<?php

namespace App\Http\Api\V1\Settings\Iku;

use App\Http\Api\V1\Settings\Iku\Requests\IkuRequestUpdate;
use App\Http\Api\V1\Settings\Iku\Requests\IkuRequestCreate;
use App\Http\Api\V1\Settings\Iku\Resources\IkuResource;
use App\Http\Controllers\Controller;
use App\Models\SettingIku;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IkuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingIku::all()
            : SettingIku::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return IkuResource::collection($action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IkuRequestCreate $request)
    {
        $action = SettingIku::create($request->validated());
        return (new IkuResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SettingIku  $iku
     * @return \Illuminate\Http\Response
     */
    public function show(SettingIku $iku)
    {
        return new IkuResource($iku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SettingIku  $iku
     * @return \Illuminate\Http\Response
     */
    public function update(IkuRequestUpdate $request, SettingIku $iku)
    {
        $iku->updateOrFail($request->validated());
        return new IkuResource($iku);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SettingIku  $iku
     * @return \Illuminate\Http\Response
     */
    public function destroy(SettingIku $iku)
    {
        $iku->delete();
        return response(['message' => 'Success delete']);
    }
}
