<?php

namespace App\Http\Api\V1\Settings\Iku\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IkuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'detail' => $this->detail
        ];
    }
}
