<?php

namespace App\Http\Api\V1\Settings\FlagshipNasional;

use App\Models\SettingFlagshipNasional;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Api\V1\Settings\FlagshipNasional\Resources\FlagshipNasionalResource;
use App\Http\Api\V1\Settings\FlagshipNasional\Resources\FlagshipNasionalCollection;
use App\Http\Api\V1\Settings\FlagshipNasional\Requests\FlagshipNasionalRequestCreate;
use App\Http\Api\V1\Settings\FlagshipNasional\Requests\FlagshipNasionalRequestUpdate;
use Illuminate\Http\Request;

class FlagshipNasionalController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingFlagshipNasional::all()
            : SettingFlagshipNasional::paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return new FlagshipNasionalCollection($action);
    }

    public function store(FlagshipNasionalRequestCreate $request)
    {
        $action = SettingFlagshipNasional::create($request->all());
        return (new FlagshipNasionalResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingFlagshipNasional $flagship_nasional)
    {
        return new FlagshipNasionalResource($flagship_nasional);
    }

    /**
     * @throws \Throwable
     */
    public function update(FlagshipNasionalRequestUpdate $request, SettingFlagshipNasional $flagship_nasional)
    {
        // dd($request->all());
        $flagship_nasional->updateOrFail($request->all());
        return new FlagshipNasionalResource($flagship_nasional);
    }

    public function destroy(SettingFlagshipNasional $flagship_nasional)
    {
        $flagship_nasional->delete();
        return response(['message' => 'Success delete']);
    }
}
