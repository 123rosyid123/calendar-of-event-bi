<?php

namespace App\Http\Api\V1\Settings\FlagshipNasional\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FlagshipNasionalRequestCreate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'  => 'required',
        ];
    }
}
