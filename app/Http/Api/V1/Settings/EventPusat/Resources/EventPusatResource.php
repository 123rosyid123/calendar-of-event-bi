<?php

namespace App\Http\Api\V1\Settings\EventPusat\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventPusatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "nama_event" => $this->nama_event,
            "waktu_acara" => $this->waktu_acara,
            "rangkaian_event" => $this->rangkaian_event,
            "id" => $this->id
        ];
    }
}
