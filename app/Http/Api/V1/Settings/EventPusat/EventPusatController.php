<?php

namespace App\Http\Api\V1\Settings\EventPusat;

use App\Http\Api\V1\Settings\EventPusat\Requests\EventPusatRequestCreate;
use App\Http\Api\V1\Settings\EventPusat\Requests\EventPusatRequestUpdate;
use App\Http\Api\V1\Settings\EventPusat\Resources\EventPusatResource;
use Illuminate\Http\Request;
use App\Models\SettingEventPusat;
use App\Http\Controllers\ApiController;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class EventPusatController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $event = Event::where('event_status', Event::HLE);
        $action = $per_page == -1
            ? $event->get()
            : $event->paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return EventPusatResource::collection($action);
    }

    public function store(EventPusatRequestCreate $request)
    {
        $validated = $request->safe()->merge([
            // 'nama_event' => $request->name,
            // 'waktu_acara' => $request->date,
            'user_id' => Auth::user()->id,
            'event_status' => Event::HLE
        ])->all();

        $event = Event::create($validated);
        // $action = SettingEventPusat::create(array_merge($validated, ['event_id' => $event->id]));

        return (new EventPusatResource($event))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Event $event_pusat)
    {
        return new EventPusatResource($event_pusat);
    }

    public function update(EventPusatRequestUpdate $request, Event $event_pusat)
    {
        $validated = $request->validated();
        $event_pusat->updateOrFail($validated);
        // $event_pusat->event->updateOrFail($validated);
        return new EventPusatResource($event_pusat);
    }

    public function destroy(Event $event_pusat)
    {
        $event_pusat->delete();
        return response(['message' => 'Success delete']);
    }
}
