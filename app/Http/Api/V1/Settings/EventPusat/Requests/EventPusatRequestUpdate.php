<?php

namespace App\Http\Api\V1\Settings\EventPusat\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventPusatRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $hidden_field = [];
        if ($this->name) {
            $hidden_field['nama_event'] = $this->name;
        }
        if ($this->date) {
            $hidden_field['waktu_acara'] = $this->date;
        }
        $this->merge($hidden_field);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rangkaian_event' => 'sometimes',
            'nama_event' => 'sometimes',
            'waktu_acara' => 'sometimes',
        ];
    }
}
