<?php

namespace App\Http\Api\V1\Settings\EventPusat\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class EventPusatRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_event' => 'required',
            'waktu_acara' => ['required', 'date'],
            'rangkaian_event' => ['required']
        ];
    }
}
