<?php

namespace App\Http\Api\V1\Settings\ChannelPublikasi;

use App\Http\Api\V1\Settings\ChannelPublikasi\Requests\ChannelPublikasiRequestCreate;
use App\Http\Api\V1\Settings\ChannelPublikasi\Requests\ChannelPublikasiRequestUpdate;
use App\Http\Api\V1\Settings\ChannelPublikasi\Resources\ChannelPublikasiResource;
use App\Models\SettingChannelPublikasi;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Repositories\Modul\ChannelPublikasiRepository;
use Symfony\Component\HttpFoundation\Response;

class ChannelPublikasiController extends ApiController
{
    public function __construct()
    {
        $this->channelPublikasiRepository = new ChannelPublikasiRepository;
    }

    public function index(Request $request)
    {
        $action = $this->channelPublikasiRepository->index($request);
        return ChannelPublikasiResource::collection($action);
    }

    public function store(ChannelPublikasiRequestCreate $request)
    {
        $action = SettingChannelPublikasi::create($request->validated());
        return (new ChannelPublikasiResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingChannelPublikasi $channel_publikasi)
    {
        return new ChannelPublikasiResource($channel_publikasi);
    }

    public function update(ChannelPublikasiRequestUpdate $request, SettingChannelPublikasi $channel_publikasi)
    {
        $channel_publikasi->updateOrFail($request->all());
        return new ChannelPublikasiResource($channel_publikasi);
    }

    public function destroy(SettingChannelPublikasi $channel_publikasi)
    {
        $channel_publikasi->delete();
        return response(['message' => 'Success delete']);
    }
}
