<?php

namespace App\Http\Api\V1\Settings\PaketSinergiFlagships\Resources;

use App\Http\Api\V1\Settings\FlagshipNasional\Resources\FlagshipNasionalCollection;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PaketSinergiFlagshipResource extends JsonResource
{
    protected $need_details;
    public function need_details($value)
    {
        $this->need_details = $value;
        return $this;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $result = [
            'id' => $this->id,
            'title' => $this->title,
            "date" => $this->date,
            'details' => $this->flagship_nasional->implode('title', ','),
        ];

        if ($this->need_details) {
            $result['detail_flagships'] = FlagshipNasionalCollection::make($this->flagship_nasional);
        }

        return $result;
    }

    public static function collection($resource)
    {
        return (new PaketSinergiFlagshipCollection($resource));
    }
}
