<?php

namespace App\Http\Api\V1\Settings\PaketSinergiFlagships;

use App\Http\Api\V1\Settings\FlagshipNasional\Policies\FlagshipNasionalPolicy;
use App\Http\Controllers\ApiController;
use App\Models\SettingPaketSinergiFlagship;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Api\V1\Settings\PaketSinergiFlagships\Resources\PaketSinergiFlagshipResource;
use App\Http\Api\V1\Settings\PaketSinergiFlagships\Resources\PaketSinergiFlagshipCollection;
use App\Http\Api\V1\Settings\PaketSinergiFlagships\Requests\PaketSinergiFlagshipRequestUpdate;
use App\Http\Api\V1\Settings\PaketSinergiFlagships\Requests\PaketSinergiFlagshipRequestCreate;
use App\Models\SettingFlagshipNasional;
use Illuminate\Auth\Events\Validated;
use Symfony\Component\HttpFoundation\Request;

class PaketSinergiFlagshipController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingPaketSinergiFlagship::with('flagship_nasional')->get()
            : SettingPaketSinergiFlagship::with('flagship_nasional')->paginate($per_page);

        // $action->load($action);
        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return PaketSinergiFlagshipResource::collection($action);
    }

    public function store(PaketSinergiFlagshipRequestCreate $request)
    {
        $Validated = $request->validated();
        $action = SettingPaketSinergiFlagship::create($Validated);

        // add flaship nasional to paket
        $detail_ids = SettingFlagshipNasional::whereIn('title', explode(',', $Validated['details']))->pluck('id');
        $action->syncFlagshipNasional($detail_ids);
        $action->load('flagship_nasional');

        return (new PaketSinergiFlagshipResource($action))->need_details(true)->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingPaketSinergiFlagship $paket_sinergi)
    {
        $paket_sinergi->load('flagship_nasional');
        return (new PaketSinergiFlagshipResource($paket_sinergi))->need_details(true);
    }

    /**
     * @throws \Throwable
     */
    public function update(PaketSinergiFlagshipRequestUpdate $request, SettingPaketSinergiFlagship $paket_sinergi)
    {
        $paket_sinergi->updateOrFail($request->validated());

        // sync flaship nasional to paket
        if (in_array('details', array_keys($request->validated()))) {
            $details_id = SettingFlagshipNasional::whereIn('title', explode(',', $request->Validated()['details']))->pluck('id');
            $paket_sinergi->syncFlagshipNasional($details_id);
        }
        $paket_sinergi->load('flagship_nasional');

        return (new PaketSinergiFlagshipResource($paket_sinergi))->need_details(true);
    }

    public function destroy(SettingPaketSinergiFlagship $paket_sinergi)
    {
        $paket_sinergi->delete();
        return response(['message' => 'Success delete']);
    }
}
