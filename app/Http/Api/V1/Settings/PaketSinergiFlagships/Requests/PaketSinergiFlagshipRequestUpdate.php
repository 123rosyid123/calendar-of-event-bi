<?php

namespace App\Http\Api\V1\Settings\PaketSinergiFlagships\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaketSinergiFlagshipRequestUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'  => 'sometimes|required',
            'details' => 'sometimes|required',
            // 'flagship_nasional_ids' => 'sometimes|required|array'
        ];
    }
}
