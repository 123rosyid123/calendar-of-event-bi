<?php

namespace App\Http\Api\V1\Settings\PaketSinergiFlagships\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaketSinergiFlagshipRequestCreate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'  => 'required',
            'details' => 'required',
            // 'flagship_nasional_ids' => 'required|array'
        ];
    }
}
