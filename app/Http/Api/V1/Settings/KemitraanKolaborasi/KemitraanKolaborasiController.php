<?php

namespace App\Http\Api\V1\Settings\KemitraanKolaborasi;

use App\Http\Api\V1\Settings\KemitraanKolaborasi\Requests\KemitraanKolaborasiRequestCreate;
use App\Http\Api\V1\Settings\KemitraanKolaborasi\Requests\KemitraanKolaborasiRequestUpdate;
use App\Http\Api\V1\Settings\KemitraanKolaborasi\Resources\KemitraanKolaborasiCollection;
use App\Http\Api\V1\Settings\KemitraanKolaborasi\Resources\KemitraanKolaborasiResource;
use App\Http\Controllers\ApiController;

use App\Models\SettingKemitraanKolaborasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class KemitraanKolaborasiController extends ApiController
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $kemitraan_kolaborasi = SettingKemitraanKolaborasi::whereIn('created_by', array(env('ADMIN_ID', 1), env('DR_ID', 2), Auth::user()->id));
        $action = $per_page == -1
            ? $kemitraan_kolaborasi->get()
            : $kemitraan_kolaborasi->paginate($per_page);

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return new KemitraanKolaborasiCollection($action);
    }

    public function store(KemitraanKolaborasiRequestCreate $request)
    {
        $validated_data = $request->safe()->merge([
            'created_by' => Auth::user()->id
        ]);
        $action = SettingKemitraanKolaborasi::create($validated_data->all());
        return (new KemitraanKolaborasiResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SettingKemitraanKolaborasi $kemitraan_kolaborasi)
    {
        return new KemitraanKolaborasiResource($kemitraan_kolaborasi);
    }

    public function update(KemitraanKolaborasiRequestUpdate $request, SettingKemitraanKolaborasi $kemitraan_kolaborasi)
    {
        $kemitraan_kolaborasi->updateOrFail($request->all());
        return new KemitraanKolaborasiResource($kemitraan_kolaborasi);
    }

    public function destroy(SettingKemitraanKolaborasi $kemitraan_kolaborasi)
    {
        $kemitraan_kolaborasi->delete();
        return response(['message' => 'Success delete']);
    }
}
