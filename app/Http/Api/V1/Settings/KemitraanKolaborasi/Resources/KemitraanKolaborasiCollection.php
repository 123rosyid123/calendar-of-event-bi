<?php

namespace App\Http\Api\V1\Settings\KemitraanKolaborasi\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class KemitraanKolaborasiCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
