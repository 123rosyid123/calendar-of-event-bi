<?php

namespace App\Http\Api\V1\Settings\PeriodeEvent\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PeriodeEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            "tanggal_mulai" => $this->tanggal_mulai_pendaftaran,
            "tanggal_akhir" => $this->tanggal_akhir_pendaftaran,
            "nama_periode" => $this->nama_periode
        ];
    }
}
