<?php

namespace App\Http\Api\V1\Settings\PeriodeEvent;

use App\Http\Api\V1\Settings\PeriodeEvent\Requests\PeriodeEventRequestCreate;
use App\Http\Api\V1\Settings\PeriodeEvent\Requests\PeriodeEventRequestUpdate;
use App\Http\Api\V1\Settings\PeriodeEvent\Resources\PeriodeEventResource;
use App\Models\SettingPeriodeEvent;
use App\Http\Controllers\ApiController;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class PeriodeEventController extends ApiController
{
    public function index()
    {
        $action = SettingPeriodeEvent::paginate(5);
        abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        // $active_period = SettingPeriodeEvent::latest()->first();
        return PeriodeEventResource::collection($action);
    }

    public function store(PeriodeEventRequestCreate $request)
    {
        $validated_data = $request->safe()->merge([
            "tanggal_mulai_pendaftaran" => $request->tanggal_mulai,
            "tanggal_akhir_pendaftaran" => $request->tanggal_akhir,
        ])->only(['tanggal_mulai_pendaftaran', 'tanggal_akhir_pendaftaran', 'nama_periode']);
        $action = SettingPeriodeEvent::create($validated_data);
        return (new PeriodeEventResource($action))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show($periode_event)
    {
        $periode = SettingPeriodeEvent::where('nama_periode', $periode_event)->orderBy('id', 'desc')->first();
        return new PeriodeEventResource($periode);
    }

    public function update(PeriodeEventRequestUpdate $request, $periode_event)
    {
        $periode = SettingPeriodeEvent::where('nama_periode', $periode_event)->orderBy('id', 'desc')->first();
        abort_if(!$periode, response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        $date = [];
        foreach (['tanggal_mulai', 'tanggal_akhir'] as $param) {
            if (in_array($param, array_keys($request->all()))) {
                $date[$param . '_pendaftaran'] = $request->validated()[$param];
            }
        };
        $validated_data = $request->safe()->merge($date)->only(['tanggal_mulai_pendaftaran', 'tanggal_akhir_pendaftaran', 'nama_periode']);

        $periode->updateOrFail($validated_data);
        return new PeriodeEventResource($periode);
    }

    public function destroy(SettingPeriodeEvent $periode_event)
    {
        $periode_event->delete();
        return response(['message' => 'Success delete']);
    }
}
