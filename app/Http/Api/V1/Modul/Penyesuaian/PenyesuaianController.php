<?php

namespace App\Http\Api\V1\Modul\Penyesuaian;

use App\Http\Api\V1\Modul\Penyesuaian\Requests\PenyesuaianRequest;
use App\Http\Controllers\ApiController;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Services\Modul\PenyesuaianService;
use Illuminate\Http\Request;

class PenyesuaianController extends ApiController
{
    public function __construct()
    {
        $this->penyesuaianService = new PenyesuaianService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(PenyesuaianRequest $request, Event $event)
    {
        $event = $this->penyesuaianService->update($request, $event);
        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
