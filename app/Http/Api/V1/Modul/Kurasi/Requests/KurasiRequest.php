<?php

namespace App\Http\Api\V1\Modul\Kurasi\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KurasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_approved' => 'required|boolean',
            'komentar' => 'required_if:is_approved,false'
        ];
    }
}
