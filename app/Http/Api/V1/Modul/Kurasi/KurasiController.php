<?php

namespace App\Http\Api\V1\Modul\Kurasi;

use App\Http\Api\V1\Modul\Kurasi\Mail\KurasiMail;
use App\Http\Api\V1\Modul\Kurasi\Requests\KurasiRequest;
use App\Http\Controllers\ApiController;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Models\Komentar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class KurasiController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(KurasiRequest $request, Event $event)
    {
        abort_if(!in_array($event->event_status, [Event::SUBMITTED, Event::REVISED]), 403, 'The data is not approvable');

        // $asd = json_encode([
        //     "access_token" => "2|acOAvomInTJzDYdzLtnncb7aEOlXjEXXbrYNo2xX",
        //     "user_id" => 1
        // ]);
        // $qwe = json_decode($asd);

        // dd($event);
        if ($event->event_status == Event::SUBMITTED) {
            $validated = $request->safe()->only([
                'is_approved',
                'komentar',
            ]);
            if ($validated['is_approved']) {
                $data = ['event_status' => Event::APPROVED];
            } else {
                $data = ['event_status' => Event::EVALUATED];
                if (in_array('komentar', array_keys($validated))) {
                    $komentar = Komentar::create([
                        'event_id' => $event->id,
                        'user_id' => Auth::user()->id,
                        'komentar' => $validated['komentar']
                    ]);
                }
            }
        } else {
            $asd = json_encode([
                "access_token" => "2|acOAvomInTJzDYdzLtnncb7aEOlXjEXXbrYNo2xX",
                "user_id" => 1
            ]);
            $qwe = json_decode($asd);
            dd($qwe);
        }


        // dd('asd');

        $event->update($data);
        // dd($event->komentars());

        // Mail::to('cobadumiakun@gmail.com')->send(new KurasiMail($event)); // TODO : just for testing
        Mail::to($event->user->email)->send(new KurasiMail($event));

        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
