<?php

namespace App\Http\Api\V1\Modul\Dashboard;

use App\Http\Api\V1\Modul\Pendaftaran\Requests\PendaftaranRequestCreate;
use App\Http\Api\V1\Modul\Pendaftaran\Requests\PendaftaranRequestUpdate;
use App\Http\Controllers\ApiController;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Models\Kpwbi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd(public_path());

        // $data = DB::select("SELECT COUNT(*) as asd, DATE_FORMAT(waktu_acara, '%e %c %Y') as tanggal
        //   from `events` where asd == 2 GROUP BY tanggal");

        // dd($data);
        // // $data = Event::select('select * from users where active = ?', [1])->get();
        // $data = DB::table('events')
        //     // ->select(DB::raw("count(*) as jumlah, DATE_FORMAT(waktu_acara, '%e %c %Y') as tanggal, GROUP_CONCAT(CONCAT(id, ',',nama_event,',',waktu_acara) separator '||') as detail"))
        //     ->select(DB::raw("count(*) as jumlah, DATE_FORMAT(waktu_acara, '%Y-%c-%e') as tanggal, GROUP_CONCAT(CONCAT(event_status)) as status"))
        //     ->groupBy('tanggal')
        //     ->having('jumlah', '>', 1)
        //     ->get();

        // // $columns = ['id', 'nama_event', 'waktu_acara'];
        // dd($data->pluck('tanggal'));
        // // $detail = $data->map(
        // //     function ($a) {
        // //         $temp = [];
        // //         $isinya = explode('||', $a->detail);
        // //         foreach ($isinya as $isi_list) {
        // //             $isi_detail = explode(',', $isi_list);
        // //             foreach($columns as $key=>$column){
        // //                 $temp[$column]
        // //             }
        // //         }
        // //         dd($isi);

        // // foreach($a as $isi){

        // // $b = [
        // // }


        // // );
        // dd($data);

        // $request->full;
        $per_page = $request->per_page ? $request->per_page : 5;

        // $event = Event::with('komentars')->where('kpwbi_id', 1)->orderBy('id', 'desc')->get()->unique('nama_event');
        $event = Event::with(
            'komentars',
            // 'setting_kemitraan_kolaborasi',
            // 'setting_iku',
            // 'setting_tema',
            // 'setting_flagship_nasional',
            // 'setting_kementrian',
            // 'setting_channel_publikasi'
        );

        if ($request->kpwbi) {
            $event = $event->where('kpwbi_id', $request->kpwbi)->orderBy('id', 'desc');
        }

        $action = $per_page == -1
            ? $event->get()
            : $event->paginate($per_page);

        if ($request->kpwbi) {
            $action = $action->unique('nama_event');
        }

        // abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return EventResource::collection($action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PendaftaranRequestCreate $request)
    {
        return null;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $event->load(
            'setting_kemitraan_kolaborasi',
            'setting_iku',
            'setting_tema',
            'setting_flagship_nasional',
            'setting_kementrian',
            'setting_channel_publikasi'
        );
        return new EventResource($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(PendaftaranRequestUpdate $request, Event $event)
    {
        return null;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        return response(['message' => 'Success delete']);
    }

    public function getDisabledDate(Request $request)
    {
        $data = DB::table('events')
            ->select(DB::raw("count(*) as jumlah, DATE_FORMAT(waktu_acara, '%Y-%m-%d') as tanggal, GROUP_CONCAT(CONCAT(event_status)) as status"))
            ->groupBy('tanggal')
            ->having('jumlah', '>', 1)
            ->orHaving('status', "=", "10")
            ->get();
        return response([
            "data" => $data->pluck('tanggal'),
            "total" => $data->count()
        ], Response::HTTP_OK);
    }

    public function getAvailableTime(Request $request)
    {
        if (!$request->tanggal) return response(['data' => null]);

        $waktu = Event::select(DB::raw("DATE_FORMAT(waktu_acara, '%H:%i') as waktu"))->whereDate('waktu_acara', '=', Carbon::parse($request->tanggal)->format('Y-m-d'))->pluck('waktu');
        if ($waktu->count() > 1) {
            $data = null;
        } elseif ($waktu->count() < 1) {
            $data = [
                "min" => "08:00",
                "max" => "17:00"
            ];
        } elseif ($waktu[0] >= '08:00' and $waktu[0] <= "12:00") {
            $data = [
                "min" => "13:00",
                "max" => "17:00"
            ];
        } else {
            $data = [
                "min" => "08:00",
                "max" => "12:00"
            ];
        }

        return response([
            "data" => $data
        ], Response::HTTP_OK);
    }
}
