<?php

namespace App\Http\Api\V1\Modul\Pendaftaran\Requests;

use App\Models\SettingSyaratPengusulanAdg;
use App\Rules\AvailableDate;
use App\Rules\ValidWithMinimumFilled;
use Illuminate\Foundation\Http\FormRequest;


class PendaftaranRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $available_date = Event::where('waktu-acara', 'b')
        $minimal_requirement = SettingSyaratPengusulanAdg::latest()->first();
        return [
            'waktu_acara' => ['required', 'date', new AvailableDate],
            'nama_kpwbi' => 'required',
            'nama_event' => 'required',
            'rangkaian_event' => 'required',
            'jumlah_pengunjung' => "numeric|min:$minimal_requirement->minimal_jumlah_pengunjung",
            'nilai_penjualan_exhibition' => ["required", "numeric", "min:" . $minimal_requirement->minimal_nilai_penjualan_exhibition],
            'nilai_penjualan_ecommerce' => "numeric|min:$minimal_requirement->minimal_nilai_penjualan_ecommerce",
            'nilai_penjualan_export' => "numeric|min:$minimal_requirement->minimal_nilai_penjulan_export",
            'nilai_business_matching_pembiayaan' => "numeric|min:$minimal_requirement->minimal_nilai_business_matching_pembiayaan",
            'nilai_business_matching_export' => "required|numeric|min:$minimal_requirement->minimal_nilai_business_matching_export",
            'onboarding_umkm_baru' => 'required',
            'perluasan_merchant_qirs' => 'required',
            'nama_iku' => 'required',
            'jumlah_iku' => 'required',
            'kehadiran_menteri' => 'required',
            'kehadiran_stakeholder_utama' => 'required',
            'kehadiran_gubernur' => 'required',
            'kehadiran_pemimpin_lembaga_pusat' => 'required',
            'is_usulan_kehadiran_adg' => [
                'sometimes', 'boolean',
                "prohibited_unless:paket_sinergi_flagship_id,4",
                "prohibited_unless:skala_event_id,2,3",
                new ValidWithMinimumFilled(1, ['kehadiran_menteri', 'kehadiran_stakeholder_utama', 'kehadiran_gubernur', 'kehadiran_pemimpin_lembaga_pusat']),
                new ValidWithMinimumFilled(6, ['jumlah_pengunjung', 'nilai_penjualan_exhibition', 'nilai_penjualan_ecommerce', 'nilai_penjualan_export', 'nilai_business_matching_pembiayaan', 'nilai_business_matching_export', 'onboarding_umkm_baru', 'perluasan_merchant_qirs']),
            ],
            'usulan_nama_adg' => 'prohibited_unless:is_usulan_kehadiran_adg,true',
            'anggaran_biaya' => 'required',
            'jumlah_sdm' => 'required',
            'channel_publikasi' => 'required',
            'liputan_media_nasional_lokal' => 'required',
            'liputan_media_internal' => 'required',
            'kehadiran_undangan_internal' => 'required',
            // 'dokumen_tor' => 'required|file|mimes:doc,docx,pdf|max:2048',

            'paket_sinergi_flagship_id' => 'required',
            'flagship_nasional_id' => 'required',
            'skala_event_id' => 'required',
            'tema_id' => 'required',

            'kemitraan_kolaborasi_ids' => 'required',
        ];
    }
}
