<?php

namespace App\Http\Api\V1\Modul\Pendaftaran\Requests;

use App\Models\SettingSyaratPengusulanAdg;
use App\Rules\AvailableDate;
use App\Rules\ValidWithMinimumFilled;
use Illuminate\Foundation\Http\FormRequest;

class PendaftaranRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $minimal_requirement = SettingSyaratPengusulanAdg::latest()->first();
        return [
            'waktu_acara' => ['sometimes', 'date', new AvailableDate],
            'nama_kpwbi' => 'sometimes',
            'nama_event' => 'sometimes',
            'rangkaian_event' => 'sometimes',
            'jumlah_pengunjung' => "numeric|min:$minimal_requirement->minimal_jumlah_pengunjung",
            'nilai_penjualan_exhibition' => ["sometimes", "numeric", "min:" . $minimal_requirement->minimal_nilai_penjualan_exhibition],
            'nilai_penjualan_ecommerce' => "numeric|min:$minimal_requirement->minimal_nilai_penjualan_ecommerce",
            'nilai_penjualan_export' => "numeric|min:$minimal_requirement->minimal_nilai_penjulan_export",
            'nilai_business_matching_pembiayaan' => "numeric|min:$minimal_requirement->minimal_nilai_business_matching_pembiayaan",
            'nilai_business_matching_export' => "sometimes|numeric|min:$minimal_requirement->minimal_nilai_business_matching_export",
            'onboarding_umkm_baru' => 'sometimes',
            'perluasan_merchant_qirs' => 'sometimes',
            'nama_iku' => 'sometimes',
            'jumlah_iku' => 'sometimes',
            'kehadiran_menteri' => 'sometimes',
            'kehadiran_stakeholder_utama' => 'sometimes',
            'kehadiran_gubernur' => 'sometimes',
            'kehadiran_pemimpin_lembaga_pusat' => 'sometimes',
            'is_usulan_kehadiran_adg' => [
                'sometimes', 'boolean',
                "prohibited_unless:paket_sinergi_flagship_id,4",
                "prohibited_unless:skala_event_id,2,3",
                new ValidWithMinimumFilled(1, ['kehadiran_menteri', 'kehadiran_stakeholder_utama', 'kehadiran_gubernur', 'kehadiran_pemimpin_lembaga_pusat']),
                new ValidWithMinimumFilled(6, ['jumlah_pengunjung', 'nilai_penjualan_exhibition', 'nilai_penjualan_ecommerce', 'nilai_penjualan_export', 'nilai_business_matching_pembiayaan', 'nilai_business_matching_export', 'onboarding_umkm_baru', 'perluasan_merchant_qirs']),
            ],
            'usulan_nama_adg' => 'sometimes|prohibited_unless:is_usulan_kehadiran_adg,true',
            'anggaran_biaya' => 'sometimes',
            'jumlah_sdm' => 'sometimes',
            'channel_publikasi' => 'sometimes',
            'liputan_media_nasional_lokal' => 'sometimes',
            'liputan_media_internal' => 'sometimes',
            'kehadiran_undangan_internal' => 'sometimes',
            // 'dokumen_tor' => 'sometimes|file|mimes:doc,docx,pdf|max:2048',

            'paket_sinergi_flagship_id' => 'sometimes',
            'flagship_nasional_id' => 'sometimes',
            'skala_event_id' => 'sometimes',
            'tema_id' => 'sometimes',

            'kemitraan_kolaborasi_ids' => 'sometimes|array',
        ];
    }
}
