<?php

namespace App\Http\Api\V1\Modul\Pendaftaran;

use App\Http\Api\V1\Modul\Pendaftaran\Requests\PendaftaranCheckRequest;
use App\Http\Api\V1\Modul\Pendaftaran\Requests\PendaftaranRequestCreate;
use App\Http\Api\V1\Modul\Pendaftaran\Requests\PendaftaranRequestUpdate;
use App\Http\Controllers\ApiController;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Services\Modul\PendaftaranService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PendaftaranController extends ApiController
{

    public function __construct()
    {
        $this->pendaftaranService = new PendaftaranService;
    }

    //checking event register validation
    public function check_validation(PendaftaranCheckRequest $request)
    {
        return response(['message' => 'validation passed'], 200);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $action = $this->pendaftaranService->index($request);
        return EventResource::collection($action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PendaftaranRequestCreate $request)
    {
        $event = $this->pendaftaranService->store($request);
        return (new EventResource($event->load(
            'setting_kemitraan_kolaborasi',
            'setting_iku',
            'setting_tema',
            'setting_flagship_nasional',
            'setting_kementrian',
            'setting_channel_publikasi'
        )))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $pendaftaran)
    {
        $pendaftaran->load('setting_kemitraan_kolaborasi', 'setting_iku', 'setting_kementrian', 'setting_channel_publikasi');
        return new EventResource($pendaftaran);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(PendaftaranRequestUpdate $request, Event $pendaftaran)
    {

        $pendaftaran = $this->pendaftaranService->update($request, $pendaftaran);
        return new EventResource($pendaftaran->refresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $pendaftaran)
    {
        $pendaftaran->delete();
        return response(['message' => 'Success delete']);
    }
}
