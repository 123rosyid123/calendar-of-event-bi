<?php

namespace App\Http\Api\V1\Modul\Pendaftaran\Requests;

use App\Models\SettingSyaratPengusulanAdg;
use App\Rules\AvailableDate;
use App\Rules\ValidWithMinimumFilled;
use Illuminate\Foundation\Http\FormRequest;


class AddPastEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $available_date = Event::where('waktu-acara', 'b')
        $minimal_requirement = SettingSyaratPengusulanAdg::latest()->first();
        return [
            'waktu_acara' => ['sometimes', 'required', 'date'],
            // 'nama_kpwbi' => 'sometimes|required',
            'nama_event' => 'sometimes|required',
            'rangkaian_event' => 'sometimes|required',
            'jumlah_pengunjung' => "numeric",
            'nilai_penjualan_exhibition' => "numeric",
            'nilai_penjualan_ecommerce' => "numeric",
            'nilai_penjualan_export' => "numeric",
            'nilai_business_matching_pembiayaan' => "numeric",
            'nilai_business_matching_export' => "required|numeric",
            'onboarding_umkm_baru' => 'sometimes|required',
            'perluasan_merchant_qirs' => 'sometimes|required',
            // 'nama_iku' => 'sometimes|required',
            // 'jumlah_iku' => 'sometimes|required',
            // 'kehadiran_menteri' => 'sometimes|required',
            // 'kehadiran_stakeholder_utama' => 'sometimes|required',
            // 'kehadiran_gubernur' => 'sometimes|required',
            // 'kehadiran_pemimpin_lembaga_pusat' => 'sometimes|required',
            // 'is_usulan_kehadiran_adg' => [
            //     'sometimes', 'required', 'boolean',
            //     "prohibited_unless:paket_sinergi_flagship_id,4",
            //     "prohibited_unless:skala_event_id,2,3",
            //     new ValidWithMinimumFilled(1, ['kehadiran_menteri', 'kehadiran_stakeholder_utama', 'kehadiran_gubernur', 'kehadiran_pemimpin_lembaga_pusat']),
            //     new ValidWithMinimumFilled(6, ['jumlah_pengunjung', 'nilai_penjualan_exhibition', 'nilai_penjualan_ecommerce', 'nilai_penjualan_export', 'nilai_business_matching_pembiayaan', 'nilai_business_matching_export', 'onboarding_umkm_baru', 'perluasan_merchant_qirs']),
            // ],
            // 'usulan_nama_adg' => 'prohibited_unless:is_usulan_kehadiran_adg,true',
            'anggaran_biaya' => 'sometimes|required',
            // 'jumlah_sdm' => 'sometimes|required',
            // 'channel_publikasi' => 'sometimes|required',
            // 'liputan_media_nasional_lokal' => 'sometimes|required',
            // 'liputan_media_internal' => 'sometimes|required',
            // 'kehadiran_undangan_internal' => 'sometimes|required',
            // 'dokumen_tor' => 'sometimes|required|file|mimes:doc,docx,pdf|max:2048',

            'paket_sinergi_flagship_id' => 'sometimes|exists:App\Models\SettingPaketSinergiFlagship,id',
            'flagship_nasional_id' => 'sometimes|exists:App\Models\SettingFlagshipNasional,id',
            'skala_event_id' => 'sometimes|exists:App\Models\SettingSkalaEvent,id',
            'tema_id' => 'sometimes|exists:App\Models\SettingTema,id',
            'kpwbi_id' => 'sometimes|exists:App\Models\Kpwbi,id',

            // 'kemitraan_kolaborasi_ids' => 'sometimes|required',
        ];
    }
}
