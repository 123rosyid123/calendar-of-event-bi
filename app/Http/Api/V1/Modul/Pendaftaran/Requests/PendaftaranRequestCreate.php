<?php

namespace App\Http\Api\V1\Modul\Pendaftaran\Requests;

use App\Models\Event;
use App\Models\SettingPeriodeEvent;
use App\Models\SettingSyaratPengusulanAdg;
use App\Rules\AvailableDate;
use App\Rules\EventTargetMinimumValue;
use App\Rules\HLEexists;
use App\Rules\ValidWithMinimumFilled;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PendaftaranRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'kpwbi_id' => Auth::user()->hasRole('KPwDN') ? Auth::user()->kpwbi_id : $this->kpwbi_id
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $minimal_requirement = SettingSyaratPengusulanAdg::latest()->first();
        return [
            'waktu_acara' => ['required', 'date', new AvailableDate],
            // 'nama_kpwbi' => 'required',
            'nama_event' => 'required',
            'rangkaian_event' => 'required',

            'jumlah_pengunjung' => "numeric|min:$minimal_requirement->minimal_jumlah_pengunjung",
            'nilai_penjualan_exhibition' => ["numeric", new EventTargetMinimumValue],
            'nilai_penjualan_ecommerce' => ["numeric", new EventTargetMinimumValue],
            'nilai_penjualan_export' => ["numeric", new EventTargetMinimumValue],
            'nilai_business_matching_pembiayaan' => ["numeric", new EventTargetMinimumValue],
            'nilai_business_matching_export' => ["numeric", new EventTargetMinimumValue],
            'onboarding_umkm_baru' => 'sometimes',
            'perluasan_merchant_qirs' => 'sometimes',

            'flagship_nasional' => 'required',
            'tema' => 'required',
            'nama_iku' => 'required',
            'jumlah_iku' => 'required',
            'kehadiran_menteri' => 'sometimes',
            'kehadiran_stakeholder_utama' => 'sometimes',
            'kehadiran_gubernur' => 'sometimes',
            'kehadiran_pemimpin_lembaga_pusat' => 'sometimes',
            'is_usulan_kehadiran_adg' => [
                'boolean',
                "prohibited_unless:paket_sinergi_flagship_id, $minimal_requirement->setting_paket_sinergi_flagship_id",
                "prohibited_unless:skala_event_id,2,3",
                new ValidWithMinimumFilled(1, ['kehadiran_menteri', 'kehadiran_stakeholder_utama', 'kehadiran_gubernur', 'kehadiran_pemimpin_lembaga_pusat']),
                new ValidWithMinimumFilled(6, ['jumlah_pengunjung', 'nilai_penjualan_exhibition', 'nilai_penjualan_ecommerce', 'nilai_penjualan_export', 'nilai_business_matching_pembiayaan', 'nilai_business_matching_export', 'onboarding_umkm_baru', 'perluasan_merchant_qirs']),
                new HLEexists
            ],
            'usulan_nama_adg' => 'prohibited_unless:is_usulan_kehadiran_adg,true',
            'anggaran_biaya_eo' => 'required',
            'anggaran_biaya_publikasi' => 'required',
            'anggaran_biaya_logistik_perlengkapan' => 'required',
            'anggaran_biaya_honor_pengisi_acara' => 'required',
            'anggaran_biaya_lainnya' => 'required',
            'jumlah_sdm_pegawai_organik' => 'required',
            'jumlah_sdm_pegawai_non_organik' => 'required',
            'jumlah_sdm_pihak_ketiga' => 'required',
            'channel_publikasi' => 'required',
            'liputan_media_nasional_lokal' => 'required',
            'liputan_media_internal' => 'required',
            'kehadiran_undangan_internal' => 'required',
            'dokumen_tor' => 'required|file|mimes:doc,docx,pdf|max:2048',
            'dokumen_pdf_tor' => 'required|file|mimes:pdf|max:2048',

            'paket_sinergi_flagship_id' => 'required|exists:App\Models\SettingPaketSinergiFlagship,id',
            // 'flagship_nasional_id' => 'required|exists:App\Models\SettingFlagshipNasional,id',
            'skala_event_id' => 'required|exists:App\Models\SettingSkalaEvent,id',
            // 'tema_id' => 'required|exists:App\Models\SettingTema,id',
            'kpwbi_id' => 'sometimes|exists:App\Models\Kpwbi,id',

            'kemitraan_kolaborasi' => 'required',
        ];
    }
}
