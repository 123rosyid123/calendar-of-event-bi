<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // dd($this->k);
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "date" => Carbon::parse($this->created_at)->format('Y-m-d H:m:s'),
            "roles" => $this->getRoleNames()->all(),
            "kpwbi" => new KpwbiResource($this->whenLoaded('kpwbi'))
        ];
    }
}
