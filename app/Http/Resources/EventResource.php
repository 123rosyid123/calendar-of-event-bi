<?php

namespace App\Http\Resources;

use App\Http\Api\V1\Settings\ChannelPublikasi\Resources\ChannelPublikasiResource;
use App\Http\Api\V1\Settings\FlagshipNasional\Resources\FlagshipNasionalResource;
use App\Http\Api\V1\Settings\Iku\Resources\IkuResource;
use App\Http\Api\V1\Settings\Kementrian\Resources\KementrianResource;
use App\Http\Api\V1\Settings\Kementrian\Resources\TemaResource;
use App\Http\Api\V1\Settings\KemitraanKolaborasi\Resources\KemitraanKolaborasiCollection;
use App\Http\Api\V1\Settings\KemitraanKolaborasi\Resources\KemitraanKolaborasiResource;
use App\Models\Event;
use App\Models\Komentar;
use App\Models\Kpwbi;
use App\Models\SettingFlagshipNasional;
use App\Models\SettingIku;
use App\Models\SettingKemitraanKolaborasi;
use App\Models\SettingTema;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'nama_kpwbi' => $this->kpwbi ? $this->kpwbi->nama : null,
            'nama_event' => $this->nama_event,
            'rangkaian_event' => $this->rangkaian_event,
            'jumlah_pengunjung' => $this->jumlah_pengunjung,
            'nilai_penjualan_exhibition' => $this->nilai_penjualan_exhibition,
            'nilai_penjualan_ecommerce' => $this->nilai_penjualan_ecommerce,
            'nilai_penjualan_export' => $this->nilai_penjualan_export,
            'nilai_business_matching_pembiayaan' => $this->nilai_business_matching_pembiayaan,
            'nilai_business_matching_export' => $this->nilai_business_matching_export,
            'onboarding_umkm_baru' => $this->onboarding_umkm_baru,
            'perluasan_merchant_qirs' => $this->perluasan_merchant_qirs,
            'nama_iku' => $this->nama_iku,
            'jumlah_iku' => $this->jumlah_iku,
            'kehadiran_menteri' => $this->kehadiran_menteri,
            'kehadiran_stakeholder_utama' => $this->kehadiran_stakeholder_utama,
            'kehadiran_gubernur' => $this->kehadiran_gubernur,
            'kehadiran_pemimpin_lembaga_pusat' => $this->kehadiran_pemimpin_lembaga_pusat,
            'is_usulan_kehadiran_adg' => $this->is_usulan_kehadiran_adg,
            'usulan_nama_adg' => $this->usulan_nama_adg,
            'anggaran_biaya_eo' => $this->anggaran_biaya_eo,
            'anggaran_biaya_publikasi' => $this->anggaran_biaya_publikasi,
            'anggaran_biaya_logistik_perlengkapan' => $this->anggaran_biaya_logistik_perlengkapan,
            'anggaran_biaya_honor_pengisi_acara' => $this->anggaran_biaya_honor_pengisi_acara,
            'anggaran_biaya_lainnya' => $this->anggaran_biaya_lainnya,
            'jumlah_sdm_pegawai_organik' => $this->jumlah_sdm_pegawai_organik,
            'jumlah_sdm_pegawai_non_organik' => $this->jumlah_sdm_pegawai_non_organik,
            'jumlah_sdm_pihak_ketiga' => $this->jumlah_sdm_pihak_ketiga,
            'channel_publikasi' => $this->channel_publikasi,
            'liputan_media_nasional_lokal' => $this->liputan_media_nasional_lokal,
            'liputan_media_internal' => $this->liputan_media_internal,
            'kehadiran_undangan_internal' => $this->kehadiran_undangan_internal,
            'dokumen_tor' => $this->dokumen_name,
            'dokumen_url' => !$this->dokumen_path ? $this->dokumen_path : Storage::url($this->dokumen_path),
            'dokumen_pdf_tor' => $this->dokumen_pdf_name,
            'dokumen_pdf_url' => !$this->dokumen_pdf_path ? $this->dokumen_pdf_path : Storage::url($this->dokumen_pdf_path),
            'paket_sinergi_flagship_id' => $this->setting_paket_sinergi_flagship_id,
            // 'flagship_nasional_id' => $this->setting_flagship_nasional_id,
            'skala_event_id' => $this->setting_skala_event_id,
            // 'tema_id' => $this->setting_tema_id,
            'waktu_acara' => Carbon::parse($this->waktu_acara)->format('Y-m-d H:m:s'),
            'event_status' => Event::status_event[$this->event_status],

            'kemitraan_kolaborasi' => KemitraanKolaborasiResource::collection(($this->whenLoaded('setting_kemitraan_kolaborasi'))),
            'tema_details' => TemaResource::collection(($this->whenLoaded('setting_tema'))),
            'flagship_nasional_details' => FlagshipNasionalResource::collection(($this->whenLoaded('setting_flagship_nasional'))),
            'iku_details' => IkuResource::collection(($this->whenLoaded('setting_iku'))),
            'kementrian_details' => KementrianResource::collection(($this->whenLoaded('setting_kementrian'))),
            'channel_publikasi_details' => ChannelPublikasiResource::collection(($this->whenLoaded('setting_channel_publikasi'))),

            'komentar' => KomentarResource::collection(($this->whenLoaded('komentars'))),
            'kpwbi' => new KpwbiResource($this->whenLoaded('kpwbi'))
            // 'setting_periode_event_id' => $this->setting_periode_event_id
        ];
    }
}
