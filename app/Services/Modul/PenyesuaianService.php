<?php
namespace App\Services\Modul;

use App\Http\Api\V1\Modul\Kurasi\Mail\KurasiMail;
use App\Models\Event;
use App\Repositories\Modul\PenyesuaianRepository;
use Illuminate\Support\Facades\Mail;

class PenyesuaianService
{

    public function __construct()
    {
        $this->penyesuaianRepository = new PenyesuaianRepository;
    }

    public function update($request, $event)
    {
        abort_if($event->event_status != Event::EVALUATED, 403, 'The data is not Editable');

        $data = $request->validated();
        $event = $this->penyesuaianRepository->temp_update($data,$event);

        Mail::to('cobadumiakun@gmail.com')->send(new KurasiMail($event)); // TODO : just for testing
        // Mail::to($event->user()->email)->send(new KurasiMail($event));       =
        return $event;

    }
}