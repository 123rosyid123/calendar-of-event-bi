<?php
namespace App\Services\Modul;

use App\Http\Api\V1\Modul\Pendaftaran\Mail\RegistrasiMail;
use App\Models\Event;
use App\Models\SettingPeriodeEvent;
use App\Models\User;
use App\Repositories\Modul\PendaftaranRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PendaftaranService
{

    public function __construct()
    {
        $this->pendaftaranRepository = new PendaftaranRepository;
    }

    public function index($request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $this->pendaftaranRepository->index($per_page);   
        return $action;
    }

    public function store($request)
    {
        $data = $request->safe()->merge([
            'event_status' => Event::SUBMITTED,
            'setting_periode_event_id' => SettingPeriodeEvent::where('nama_periode', 'pendaftaran')->latest()->first()->id,
            'setting_paket_sinergi_flagship_id' => $request->paket_sinergi_flagship_id,
            'setting_flagship_nasional_id' => $request->flagship_nasional_id,
            'setting_skala_event_id' => $request->skala_event_id,
            'setting_tema_id' => $request->tema_id,
            'user_id' => Auth::user()->id,
        ])->except([
            'paket_sinergi_flagship_id',
            'flagship_nasional_id',
            'skala_event_id',
            'tema_id'
        ]);

        $event = $this->pendaftaranRepository->store($data, $request); 

        Mail::to(Auth::user()->email)
            ->cc(User::role(['DR', 'Admin'])->get()->toArray())
            ->send(new RegistrasiMail($event));
        // Mail::to(User::role(['DR', 'Admin'])->get()->toArray())->send(new RegistrasiMailAdmin($event));
        return $event;

    }

    public function update($request, $pendaftaran)
    {
        // dd($pendaftaran);
        $pendaftaran->load('setting_kemitraan_kolaborasi', 'setting_iku');
        $foreign_ids = [];
        foreach (['paket_sinergi_flagship_id', 'flagship_nasional_id', 'skala_event_id', 'tema_id'] as $param) {
            if (in_array($param, array_keys($request->all()))) {
                $foreign_ids['setting_' . $param] = $request->validated()[$param];
            }
        };
        $data = $request->safe()->merge($foreign_ids)->except([
            'paket_sinergi_flagship_id',
            'flagship_nasional_id',
            'skala_event_id',
            'tema_id'
        ]);

        // dd($request->all());

        $pendaftaran->update($data);
        // upload document
        if ($request->hasFile('dokumen_tor')) {
            $dokumen_name = $request->file('dokumen_tor')->getClientOriginalName();
            $file_name = sha1(rand()) . '_' . str_replace(' ', '_', $dokumen_name);
            $dokumen_path = request()->file('dokumen_tor')->storeAs('doc', '_' . ($request->user()->id ?? 'anonim') . '/' . $file_name, 'public_storage');

            Storage::delete($pendaftaran->document_path);
            $pendaftaran->update(['dokumen_name' => $dokumen_name, 'dokumen_path' => $dokumen_path]);
        }
        // upload pdf
        if ($request->hasFile('dokumen_pdf_tor')) {
            $dokumen_pdf_name = $request->file('dokumen_pdf_tor')->getClientOriginalName();
            $file_pdf_name = sha1(rand()) . '_' . str_replace(' ', '_', $dokumen_pdf_name);
            $dokumen_pdf_path = request()->file('dokumen_pdf_tor')->storeAs('pdf', '_' . $request->user()->id . '/' . $file_pdf_name, 'public_storage');
            Storage::delete($pendaftaran->document_pdf_path);
            $pendaftaran->update([
                'dokumen_pdf_name' => $dokumen_pdf_name,
                'dokumen_pdf_path' => $dokumen_pdf_path
            ]);
        }
        
        $pendaftaran = $this->pendaftaranRepository->update($request, $pendaftaran); 

        return $pendaftaran;

    }
}