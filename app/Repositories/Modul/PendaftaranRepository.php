<?php

namespace App\Repositories\Modul;

use App\Models\Event;
use Symfony\Component\HttpFoundation\Response;
use App\Models\SettingChannelPublikasi;
use App\Models\SettingFlagshipNasional;
use App\Models\SettingIku;
use App\Models\SettingKementrian;
use App\Models\SettingKemitraanKolaborasi;
use App\Models\SettingTema;
use Illuminate\Support\Facades\Auth;

class PendaftaranRepository
{
    public function index($per_page){
        $events = Event::with('komentars', 'kpwbi');
        $action = $per_page == -1
            ? $events->get()
            : $events->paginate($per_page);

        abort_if($action->isEmpty(), response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND));
        return $action;
    }

    public function store($data, $request)
    {
        $kemitraan_kolaborasi_names = array_unique(explode(',', $data['kemitraan_kolaborasi']));

        foreach ($kemitraan_kolaborasi_names as $kemitraan_kolaborasi) {
            SettingKemitraanKolaborasi::firstOrCreate(['title' => $kemitraan_kolaborasi], [
                'created_by' => Auth::user()->id
            ]);
        }

        $event = Event::create($data);

        // add kemitraan kolaborasi to event
        $kemitraan_kolaborasi_ids = SettingKemitraanKolaborasi::whereIn('title', $kemitraan_kolaborasi_names)->pluck('id');
        $event->sync_kemitraan_kolaborasi($kemitraan_kolaborasi_ids);

        // add tema to event
        $tema_names = array_unique(explode(',', $data['tema']));
        $tema_ids = SettingTema::whereIn('title', $tema_names)->pluck('id');
        $event->sync_tema($tema_ids);

        // add flaghsip_nasional to event
        $flagship_nasional_names = array_unique(explode(',', $data['flagship_nasional']));
        $flagship_nasional_ids = SettingFlagshipNasional::whereIn('title', $flagship_nasional_names)->pluck('id');
        $event->sync_flagship_nasional($flagship_nasional_ids);

        // add IKU to event
        $iku_names = array_unique(explode(',', $data['nama_iku']));
        $iku_ids = SettingIku::whereIn('nama', $iku_names)->pluck('id');
        $event->sync_iku($iku_ids);

        // add Kementrian to event
        $kementrian_names = array_unique(explode(',', $data['kehadiran_menteri']));
        $kementrian_ids = SettingKementrian::whereIn('name', $kementrian_names)->pluck('id');
        $event->sync_kementrian($kementrian_ids);

        // add Channel Publikasi to event
        $channel_publikasi_names = array_unique(explode(',', $data['channel_publikasi']));
        $channel_publikasi_ids = SettingChannelPublikasi::whereIn('name', $channel_publikasi_names)->pluck('id');
        $event->sync_channel_publikasi($channel_publikasi_ids);

        // upload document 
        if ($request->hasFile('dokumen_tor')) {
            $dokumen_name = $request->file('dokumen_tor')->getClientOriginalName();
            $file_name = sha1(rand()) . '_' . str_replace(' ', '_', $dokumen_name);
            $dokumen_path = request()->file('dokumen_tor')->storeAs('doc', '_' . $request->user()->id . '/' . $file_name, 'public_storage');
            $event->update([
                'dokumen_name' => $dokumen_name,
                'dokumen_path' => $dokumen_path
            ]);
        }
        // upload pdf
        if ($request->hasFile('dokumen_pdf_tor')) {
            $dokumen_pdf_name = $request->file('dokumen_pdf_tor')->getClientOriginalName();
            $file_pdf_name = sha1(rand()) . '_' . str_replace(' ', '_', $dokumen_pdf_name);
            $dokumen_pdf_path = request()->file('dokumen_pdf_tor')->storeAs('pdf', '_' . $request->user()->id . '/' . $file_pdf_name, 'public_storage');
            $event->update([
                'dokumen_pdf_name' => $dokumen_pdf_name,
                'dokumen_pdf_path' => $dokumen_pdf_path
            ]);
        }
        
        return $event;
    }

    public function update($request, $pendaftaran)
    {
        // sync IKU to event
        if ($request->nama_iku) {
            $iku_names = array_unique(explode(',', $request->nama_iku));
            $iku_ids = SettingIku::whereIn('nama', $iku_names)->pluck('id');
            $pendaftaran->sync_iku($iku_ids);
            // dd($pendaftaran->setting_iku);
        }
        return $pendaftaran;
    }

}