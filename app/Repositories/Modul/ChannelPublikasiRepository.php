<?php

namespace App\Repositories\Modul;

use App\Models\SettingChannelPublikasi;

class ChannelPublikasiRepository
{

    public function index($request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $action = $per_page == -1
            ? SettingChannelPublikasi::all()
            : SettingChannelPublikasi::paginate($per_page);

        return $action;
    }

}