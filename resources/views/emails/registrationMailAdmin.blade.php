<!DOCTYPE html>
<html>

<head>
    <title>Thanks for Registration </title>

    <style>
        .text-review-left {
            font-weight: 400;
            color: #2f2f30;
            display: grid;
        }

        .text-review-right {
            font-weight: 400;
            display: grid;
        }

        .text-review-title {
            font-weight: 600;
            color: #2f2f30;
            padding-bottom: 15px;
            padding-top: 20px;
        }

        .leftbox-review {
            float: left;
            margin-right: 10px;
            width: 250px;
        }

        .itembox-review {
            margin-bottom: 15px;
            padding-left: 16px;
            overflow: hidden;
        }

        .reviewgroup {
            padding: 0;
        }

        .colon-review {
            float: left;
            margin-right: 5px;
        }

    </style>
</head>

<body>


    <div style="padding: 0 40px 20px 40px">
        <h4 class="text-review-title">1. Informasi Event</h4>
        <div class="review-group">
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Tanggal Event</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->waktu_acara }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Waktu Event</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->waktu_acara }} WIB
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Nama KPwIB</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">{{ $detail->kpwbi->nama }}</h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Rangkaian Event</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right large-text">
                    {{ $detail->rangkaian_event }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">
                        Paket Sinergi Flagship Program
                    </h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->paket_sinergi_flagship->title }}:
                    {{ $detail->paket_sinergi_flagship->flagship_nasional->implode('title', ', ') }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">
                        Keterkaitan Flagship Nasional
                    </h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->flagship_nasional->title }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Tema</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">{{ $detail->setting_tema->title }}</h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Skala Event</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">{{ $detail->kpwbi->nama }}</h4>
            </div>
        </div>

        <h4 class="text-review-title">2. Target Event</h4>
        <div class="review-group">
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Jumlah Pengunjung</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->jumlah_pengunjung }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Nilai penjualan exhibition</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->nilai_penjualan_exhibition }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Nilai penjualan e-commerce</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->nilai_penjualan_ecommerce }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Nilai penjualan export</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right large-text">
                    {{ $detail->nilai_penjualan_export }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">
                        Nilai business matching pembiayaan
                    </h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->nilai_business_matching_pembiayaan }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">
                        Nilai business matching ekspor
                    </h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->nilai_business_matching_export }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">
                        Onboarding UMKM baru di e-commerce
                    </h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->onboarding_umkm_baru }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">
                        Perluasan merchant QRIS baru pada event
                    </h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->perluasan_merchant_qirs }}
                </h4>
            </div>
        </div>
        <h4 class="text-review-title">3. SDM</h4>
        <div class="review-group">
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Nama IKU</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">{{ $detail->nama_iku }}</h4>
                $detail->nama_iku
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Jumlah IKU</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">{{ $detail->jumlah_iku }}</h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Kehadiran Menteri</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->kehadiran_menteri }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Kehadiran Stackholder Utama</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right large-text">
                    {{ $detail->kehadiran_stakeholder_utama }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Kehadiran Gubernur</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->kehadiran_gubernur }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">
                        Kehadiran Pemimpin Lembaga Pusat
                    </h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->kehadiran_pemimpin_lembaga_pusat }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Usulan Kehadiran ADG</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->is_usulan_kehadiran_adg ? 'YA' : 'TIDAK' }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Usulan Nama ADG</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->usulan_nama_adg }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Anggaran Biaya</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{-- {{ $detail->anggaran_biaya }} --}}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Jumlah SDM Terlibat</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                {{-- <h4 class="text-review-right">{{ $detail->jumlah_sdm }}</h4> --}}
            </div>
        </div>

        <h4 class="text-review-title">4. Publikasi</h4>
        <div class="review-group">
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Chanel Publikasi</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->channel_publikasi }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Liputan Media</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ explode('-', $detail->liputan_media_nasional_lokal)[0] }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Jumlah Liputan Media</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ explode('-', $detail->liputan_media_nasional_lokal)[1] }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Liputan Media Internal</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->liputan_media_internal }}
                </h4>
            </div>
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Kehadiran Undangan Internal</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right large-text">
                    {{ $detail->kehadiran_undangan_internal }}
                </h4>
            </div>

        </div>

        <h4 class="text-review-title">5. Kemitraan dan Kolaborasi</h4>
        <div class="review-group">
            <div class="itembox-review">
                <div class="leftbox-review">
                    <h4 class="text-review-left">Kemitraan dan Kolaborasi</h4>
                </div>
                <h4 class="colon-review text-review-right">:</h4>
                <h4 class="text-review-right">
                    {{ $detail->setting_kemitraan_kolaborasi->implode('title', ', ') }}
                </h4>
            </div>
        </div>
    </div>


</body>

</html>
